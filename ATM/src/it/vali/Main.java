package it.vali;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Main {
    int originalPin = Integer.parseInt(loadPin());

    public static void main(String[] args) {
//	sisesta PIN 1234
//	Vali toiming a.SS b.SV C. kontojääk d. muuda PIN
//   Valiti b:
//        vali summa: 5€, 10€, 20€, muu summa
//        sisesta summa
//        Kui valiti d: sisesta PIN 1234, sisesta uusPIN 2345
//        Sisesta uusPIN uuesti 2345, vastus PIN muudetud.

//        hoiame pin koodi failis pin.txt
//        kontojääk balance.txt


        int originalPin = Integer.parseInt(loadPin());
//        raha välja võtmine
        int balance = loadBalance();
//        lisa kontoväljavõte funktsionaalsus
//        kellaaja küsimine
//        Date date = new Date();
//        DateFormat dateFormat= new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//        Calendar cal = Calendar.getInstance();
//        System.out.println(dateFormat.format(date));
//        System.out.println(date);
//        lisa KVV funktsionaalsus


//amount muutuja, mis ütleb, mis summa kasutaja välja võtis
//        Withdraw


        System.out.println("Sisesta PIN ");
        Scanner scanner = new Scanner(System.in);


        int pin = Integer.parseInt(scanner.nextLine());

        if (originalPin != pin) {
            System.out.println("Sisestatud PIN on vale, Kaart konfiskeeritud");

            return;
        }
//        siin võiks olla : sisesta pin uuesti (kuni 3 korda)

//        võimalik void meetodit lõpetada, RETURN lõpetab tööd
//        BREAK väljub tsüklist; RETURN väljub meetodist


        System.out.println("Vali toiming ");
        System.out.println("a:Sularaha sissemakse");
        System.out.println("b:SV");
        System.out.println("c:Kontojääk");
        System.out.println("d:PIN muutmine");
        System.out.println("e: Konto väljavõte");
        String toiming = scanner.nextLine();


        if (toiming.equals("a")) {
            System.out.println("Sisesta kupüürid ");
        } else if (toiming.equals("b")) {
            System.out.println("Sisesta summa ");
            System.out.println("a.5");
            System.out.println("b.10");
            System.out.println("c.20");
            System.out.println("d:muu summa");
            int summa = 0;
            String valik = scanner.nextLine();
            switch (valik) {
                case "a":
                    summa = 5;
                    break;
                case "b":
                    summa = 10;
                    break;
                case "c":
                    summa = 20;
                    break;
                case "d":
                    System.out.println("Sisesta summa");
                    String muusumma = scanner.nextLine();
                    summa = Integer.parseInt(muusumma);
                    break;
            }
            balance = balance - summa;
            saveBalance(balance);
//            saveTransaction(balance);
//            saveTransaction (amont,"deposit");
//            break;


//balance miinus muusumma
        } else if (toiming.equals("c")) {
            loadBalance();
            System.out.println("Kontojääk on " + loadBalance() + "€");

        } else if (toiming.equals("d")) {
            System.out.println("Sisesta uus PIN ");
            scanner = new Scanner(System.in);
            String uuspin = scanner.nextLine();
            savePin(uuspin);
            System.out.println("Uus Pin " + loadPin() + " on salvestatud ");

        } else if (toiming.equals("e")) {
            System.out.println("Sisesta periood");
            scanner = new Scanner(System.in);
            currentDateTimeToString();

        }

    }


    static String currentDateTimeToString() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
//        move calendar to yesterday
        calendar.add(Calendar.DATE, -1);

        System.out.println(dateFormat.format(date));
        return dateFormat.format(date);

    }


    static void savePin(String pin) {
        try {
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("pin koodi salvestamibe ebaõnnestus");

        }
//        toimub PIN kirjutamine pin.txt faili
    }

    static String loadPin() {
//        Shadowing on võimalik kasutada sama muutuja kaks  ja enam korda, tuleb panna def This. muutuja
        String pin = null;
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {

        }
        return pin;
    }


    static void saveBalance(int balance) {
        try {
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");

        }

    }

    static int loadBalance() {
        int balance = 0;
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            balance = Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }
//    siin peab olema meetod, mis liidab sisestatud summa (SS) balanciga
//    static  void SaveTransaction (int amont; String transaction){
//
//        if (transaction.equals ("deposit"));
//    }

    static boolean validatePin() {
        Scanner scanner = new Scanner(System.in);
        int originalPin = Integer.parseInt(loadPin());
        for (int i = 0; i < 3; i++) {
            System.out.println("palun sisesta PIN");
            String enteredPin = scanner.nextLine();
            if (enteredPin.equals(originalPin)) {
                System.out.println("tore");
                return true;

            }
        }
        return false;
    }

    //        ekraanile väljaprindimine
//    read from file
//
    static void printTransaction() {
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = bufferedReader.readLine();
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }

            bufferedReader.close();
            fileReader.close();


        } catch (
                FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
