package it.vali;

public class Main {

    public static void main(String[] args) {
//        Abstractklass on hübriid interfeisist ja klassist
//        Selles klassis saab defineerida nii meetodite struktuure (nagu interfeisis),
//        aga saa ka defineerida meetodi koos sisuga
//        Abstraktklassist ei saa otse objekti luua, saa ainult pärineda

        ApartmentKitchen kitchen = new ApartmentKitchen(); {
             kitchen.setHeight(100);
             kitchen.becomeDirty();
            }
        }

}
