package it.vali;

import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

//        try {
//            FileWriter fileWriter = new FileWriter("output.txt", true);
//            fileWriter.append("tere\r\n");
//            fileWriter.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        //    1.    Koosta täisarvude massiiv 10-st arvust ning seejärel kirjuta faili kõik suuremad arvud kui 2
        int[] numbers = {1, 5, 9, 22, 7, 0, 12, 55, 2, 33};

        try {
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt");
            for (int i = 0; i < numbers.length; i++) {
                if (numbers[i] > 2) {
                    fileWriter.append(Integer.toString(numbers[i]) + " ");
                }
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


//
//    2.    Kusi kasutajalt kaks arvu. Küsi seni kuni mõlemad on korrektsed (on numbriks teisendatavad).
//          Ning nii kaua ,kui nende summa ei ole paaris arv.

//        teine variant: kasutada boolean'i
//        boolean correctNumber = false;

        int summa;

        do {
            Integer a = null;
            Integer b = null;

            Scanner scanner = new Scanner(System.in);
            do {
                System.out.println("Sisesta esimene arv");
                try {
                    a = Integer.parseInt(scanner.nextLine());
//                    correctNumber =true;
                } catch (NumberFormatException e) {
                    System.out.println("Sisestasid täht");
                }
            } while (a == null);
//            while (!correctNumber);
            do {
                System.out.println("Sisesta teine arv");
                try {
                    b = Integer.parseInt(scanner.nextLine());
                } catch (NumberFormatException e) {
                    System.out.println("Sisestasid täht");

                }
            } while (b == null);
            summa = a + b;

        } while (summa % 2 != 0);


// 3. Küsi kasutajalt mitu arvu ta tahab sisestada
        // seejärel küsi ühe kaupa kasutajalt need arvud
        // ning kirjuta nende arvude summa faili, nii et see lisatakse alati juurde (Append)
        // Tulemus on iga programmi käivitamise järel on failis kõik eelnevad summad kirjas.
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mitu arvu soovid sisestada?");
        int c = Integer.parseInt(scanner.nextLine());
        int sum = 0;
        for (int i = 1; i < c + 1; i++) {
            System.out.println("Sisesta arv " + i);
            int k = Integer.parseInt(scanner.nextLine());
            sum = sum + k;
        }
        System.out.println(sum); // prindib ekraani peale

        try {
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output1.txt", true);
            fileWriter.append(Integer.toString(sum) + " ");
            fileWriter.close();
        } catch (IOException e) {

        }


    }
}
