package it.vali;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[] numbers = new int[5];
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

        }
//        aseta /lisa massiivi viis numbrit
        numbers[0] = -4;
        numbers[1] = 2;
//        numbers[2]= 0;
//        numbers[3]= 14;
//        numbers[4]= 7;
//        eemalde siis teine number
        numbers[1] = 0;
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

        }
        System.out.println();
        numbers[2] = -4;
        numbers[3] = 2;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
//        kui tahaks numbri juurde panna:
        numbers[4] = numbers[3];
        numbers[3] = 5;
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();
        numbers[3] = numbers[4];
        numbers[4] = 0;
        System.out.println();
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();
        int x = numbers[0];
        numbers[0] = numbers[3];
        numbers[3] = x;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();
//        massiivides muudatusi tegemine on keeruline
//        selleks on Collectsion, ehk ArrayListid
//        Tavalisse arraylisti võin lisada ükskõik mis tüüpi elemendid,
//        aga küsides selat pean ma teadma mis tüüpi element kus täpselt asub
//        ning pean siis selleks tüüboiks küsima ka cast'ima teisendama
        List list = new ArrayList();
//        listi saab panna ükskõik mis tüübi muutujad
//        listi suurus on nii suur nagu listis elemente on!!
        list.add("tere");
        list.add(23);
        list.add(false);

        double money = 24.55;
        Random random = new Random();

        list.add(money);
        list.add(random);
        int a = (int) list.get(1);
        String word = (String) list.get(0);
//        saab küsida mis tüüpi saa oled

            int sum =3 +(int)list.get(1);
            if (Integer.class.isInstance(a)){
                System.out.println("a on int");
            }
            if (Integer.class.isInstance(list.get(1))){
                System.out.println("listis indeksiga 1 on int");
            }
            if (String.class.isInstance(list.get(0))){
                System.out.println("listis indexiga 0 on string");
            }

        String sentence = list.get(0)+ "hommikust";
        System.out.println(sentence);

        list.size();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
//        asendamine ühe elemendi teisega
        list.add(2,"head aega");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
//
       List otherList = new ArrayList();
        otherList.add(1);
        otherList.add("maja");
        otherList.add(true);
        otherList.add(2.342333);
        list.addAll(otherList);
//        ühte listi teise sisse panemine
        list.addAll(3, otherList);
        System.out.println();
        for (int i = 0; i <list.size(); i++) {
            System.out.println(list.get(i));

        }
//        contein otsib kas on asi listis
        if (list.contains(money)){
            System.out.println("money asub listis");
        }
//        otsib kus listis objekt asub
        System.out.printf("money asub listis indeksiga %d%n",list.indexOf(money));
//       näiteks: otsib kus asub listis 23
        System.out.printf("23 asub listis indeksiga %d%n",list.indexOf(23));
//        kui ei leia tagastab -1
        System.out.printf("44 asub listis indeksiga %d%n",list.indexOf(44));
        list.indexOf(money);
//        võtta listist välja
        list.remove(money);
        list.remove(5);
        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));

        }
//        prindib välja mitu elemente listis on?
        System.out.println(list.size());
//        lisaks on võimalik filtreerida ja muud otsingud
        System.out.println();
//        list.subList(2,5);
//        for (int i = 0; i < list.size(); i++) {
//            System.out.println(list.get(i));
//
//        }
//        asendab
        list.set(0,"tore");
        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));

        }

    }

}
