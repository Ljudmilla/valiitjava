package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

//        Generic arraylist on selline kollektsioon,
//        kus loomoise hetkel peame määrama,
//        mis tüüpi elemente see sisaldama hakkab
//        List list = new ArrayList();--see on tavaline list
        List<Integer> numbers = new ArrayList<Integer>();
        numbers.add(Integer.valueOf("10"));
        numbers.add(1);
        numbers.add(2);

        List<String> words= new ArrayList<String>();
        words.add("tere");
        words.add("head aega");
    }
}
