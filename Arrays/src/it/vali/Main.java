package it.vali;

public class Main {

    public static void main(String[] args) {
	 // Järjend, massiiv, nimekiri
                //saab hoida sama tüüpi elemente

        //luuakse täisarvude massiiv millese mahub 5 elementi
        // loomise hetkel määratud elemente arvu hiljem muuta ei saa.

        int[] numbers = new int[5];
        //massiivid algavad 0st mitte 1 st
        //viimane index on alati 1 võrra väiksem kui massiivi pikkus
        numbers [0] = 2;
        numbers [1] = 7;
        numbers [2] = -2;
        numbers [3] = 11;
        numbers [4] = 1;

        System.out.println(numbers[0]);
        System.out.println(numbers[1]);
        System.out.println(numbers[2]);
        System.out.println(numbers[3]);
        System.out.println(numbers[4]);

        System.out.println();

        for (int i = 0; i < 5; i++) {
            System.out.println(numbers[i]);
        }

        System.out.println();
        //Prindi numbrid tagurpiidises järjekorras

        for (int i = 4; i >= 0; i--) {
            System.out.println(numbers[i]);

        }
        System.out.println();

        //Prindi numbrid mis on suuremad kui 2
        for (int i = 1; i < numbers.length; i++) {
                if (numbers[i] > 2) {
                    System.out.println(numbers[i]);
                }
        }
        System.out.println();
        //prindi kõik paarisarvud
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {
            System.out.println(numbers[i]);
        }
        }
        System.out.println();
        // Prindi tagant poolt 2 esimest paaritud arvu
        int counter = 0;
        for (int i = numbers.length - 1; i >= 0; i--) {
            if (numbers[i] % 2 != 0) {
                System.out.println(numbers[i]);
                counter++;
                if(counter == 2){
                    break;
                }
            }
        }

   // Loom teine massiv 3 le numbrile ja pane sinna esimesest
        // prindi teise massiivi elemendid ekraanile
        // Loo kolmas massiiv 3-le nimbrile ja pane sinna
        int[] secondNumbers = new int [3];
        for (int i = 0; i < secondNumbers.length; i++) {
            System.out.println(secondNumbers[i]);
                }

        secondNumbers[0] = numbers [0];
        secondNumbers[1] = numbers [1];
        secondNumbers[2] = numbers [2];
        for (int i = 0; i < secondNumbers.length; i++) {
            System.out.println(secondNumbers[i]);
        }
        System.out.println();
        // esimeselt massiivist 3 numbrit tagant poolt alates (1,11,-2)
//        secondNumbers[0] = numbers [4];
//        secondNumbers[1] = numbers [3];
//        secondNumbers[2] = numbers [2];

        for (int i = 0; i < secondNumbers.length; i++) {
            secondNumbers[i] = numbers[secondNumbers.length - i - 1];
        }
        for (int i = 0; i < secondNumbers.length; i++){
            System.out.println(secondNumbers[i]);
        }
        System.out.println();

        int [] thirdNumbers = new int[3];
        for (int i = 0, j = numbers.length -1; i < secondNumbers.length; i++, j--) {
            thirdNumbers[i ] = numbers[j];}

        for (int i = 0; i <  thirdNumbers.length; i++) {
            System.out.println(thirdNumbers[i]);

        }




    }
}
