package it.vali;
import java.util.List;
public class Country {
    private List<String> name;
    private String populatsion;
    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    public String getPopulatsion() {
        return populatsion;
    }

    public void setPopulatsion(String populatsion) {
        this.populatsion = populatsion;
    }

    public List<String> getName() {
        return name;
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.join(", ",language,populatsion );

    }
}
