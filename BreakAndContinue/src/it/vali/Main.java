package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Küsime kasutajalt PIN koodi
        // kui see on õige, siis ütleme "Tore"
        // kui ei  ole küsime uuesti.
        // Kokku küsime kolm korda.

        String realPin = "1234";
        Scanner scanner = new Scanner(System.in);
        // break hüppab välja tsüklist

        for (int i = 0; i < 3; i++) {

            System.out.println("Palun sisesta Pin kood");

            String enteredPin = scanner.nextLine();

            if (enteredPin.equals(realPin)) {
                System.out.println("Tore Õige Pin");
                break;

            }
        }

//        }
//        int retriesLeft = 3;
//        boolean pinWasCorrect = false;
//        String answer;
//
//        do {
//            System.out.println("Palun sisesta Pin kood");
//            retriesLeft--;
//
//        } while (!scanner.nextLine().equals(realPin) && retriesLeft > 0);
//        if () {
//            System.out.println("Panid 3 korda valesti");
//        } else {
//            System.out.println("Tore Õige Pin");
//        }


        // prindi välja 10 kuni 20 ja 40 kuni 60
        //continue jätab selle tsüklikoorduse katki ja läheb järgmise korduse juurde

        for (int i = 10; i <= 60; i++) {
            if (i > 20 && i < 40) {

                continue;
            }
            System.out.println(i);

        }
    }
}
