package it.vali;

import java.sql.SQLOutput;

public class Main {

    public static void main(String[] args) {
        /*int sum = sum(4, 5);
        System.out.printf("Arvude 4 ja 5 summa on %d%n", sum);
        System.out.printf("Arvude -12 ja -6 vahe on %d%n", sub(-12, -6));
        System.out.printf("Massiivi arvude summa on %d%n", sum);
//        System.out.printf("massiiv on ",)
//        printNumbers(reversedNumbers(reversedNumbers()));
        printNumbers(new int[]{1, 2, 3, 4});
        printNumbers(reverseNumbers(new int[]{1, 2, 3, 4}));
        sum(reverseNumbers(new int[]{1, 3}));
        printNumbers(convertToIntArray(new String[]{"2", "-12", "1", "0", "17" }));

        String[] numberAsText = new String[]{"200", "-12", "1" };
        int[] numberAsInt = convertToIntArray(numberAsText);
        printNumbers(numberAsInt);

        int[] numbers = new int[3];
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = -2;

        sum = sum(numbers);
        System.out.printf("Massiivi arvude summa on %d%n", sum);

        numbers = new int[]{2, 5, 12, -12};
        sum = sum(new int[]{2, 3, 12, 323, 43, 434, -11});
        System.out.printf("Massiivi arvude summa on %d%n", sum);
        sum = sum(new int[]{2, 3});
        */
        String[] sen = new String[]{"aa", "bb", "cc" };
        System.out.println(sentences(sen));
        System.out.println(sentencesSeparator(sen, "*"));

        int[] arvud = new int[]{1, 3, 5, 2};
        System.out.println(keskmine(arvud));
        System.out.println(protsent(5,3));
        System.out.println( circle(5));

//        int a=50;
//        int b = 56;
//        kui tehame kasutada % märku printf sees, siis kasutame topelt % ehk %%
//        Suste.out.printf ("Arv %d moodustab arvust %d % .2f%%",a b percent (a,b));
//        System.out.printf("Sõnade massiv liidetuna sõnadeks on %s%n", arrayToString ("ja", new String[]{"elas",
//                "metasa"}));

    }

    //    meetod, mis liidab kaks täisarvu kokku ja tagastab nende summa
    static int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }

    //    Subract
    static int sub(int a, int b) {
        int sub = a - b;
        return sub;
    }

    //    Multiply
    static int multiply(int a, int b) {
        return a * b;
    }

    //    divide
    static double divide(int a, int b) {
        return (double) a / b;
    }

    // Meetod, mis võtab parameetriks täisarvude massiivi ja liidab elemndid kokku ning tagastab summa
    static int sum(int[] numbers) {
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }
//    Meetod, mis võtab parameetriks täismassiivi, pöörab tagurpidi ja tagstab selle
//   1 2 3 4 5
//    5 4 3 2 1

    static int[] reverseNumbers(int[] numbers) {
        int[] reversedNumbers = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            reversedNumbers[i] = numbers[numbers.length - i - 1];


        }

        return reversedNumbers;
    }

    //    Meetod, mis prindib välja täisarvude massiivi elemendid
    static void printNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
    }

    //    meetod, mis võtab parameetriksstringi masiivi (eeldusel, et tegelikult seal massiivis on numbrid stringidena)
//    ja teisendab numbrite massiiviks ning tagastab selle
    static int[] convertToIntArray(String[] numbersAsText) {
        int[] numbers = new int[numbersAsText.length];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]);

        }

        return numbers;

    }

    //    Meetod, mis võtab parameetriks stringi massiivi ning tagastab lause, kus iga stringi vahel on tühik
//    static String arrayToString (String[] sentence) {
//    return String.join(" ", sentence);
    static String sentences(String[] words) {
        String sentence = "";

        for (int i = 0; i < words.length; i++) {
            sentence += words[i];
            if (i < words.length - 1) {
                sentence += " ";
            }
        }
        return sentence;
    }

    //    Meetod, mis võtab parameetriks stringi massiivi ning teine parameeter oleks sõnade eraldaja
//    Tagastada lause.
//    Vaata eelmist ülesannet, lihtsalt tühiku asemel saad ise valida, mille pealt sõnu eraldada.
    static String sentencesSeparator(String[] words, String separator) {
        String sentence = "";

        for (int i = 0; i < words.length; i++) {
            sentence += words[i];
            if (i < words.length - 1) {
                sentence += separator;
            }
        }
        return sentence;
    }

    //    Meetod, mis leiab numbrite massiivist keskmine ning tagastab selle
    static double keskmine(int[] arvud) {
        int sum = 0;
        for (int i = 0; i < arvud.length; i++) {
            sum = sum + arvud[i];
        }


        return (double) sum / arvud.length;
    }
//    static double
//
//   Meetod, mis arvutab mitu % moodustab esimene arv teisest.
    static double protsent(int esimene, int teine) {
        return (double) esimene * 100 / teine;
    }
//    Meetod, mis leiab ringi ümbermõõdu raadiuse järgi
    static double circle (int raadius){
        return (double) 2*Math.PI*raadius;

    }

}
//










