package it.vali;

import javax.sound.midi.Soundbank;
import java.util.Scanner;

public class Main {
//küsi kasutajalt kaks arvu
//    seejärel mis tehet ta sooviv teha
//    vali tehe:
//    a)liitmine,
//    b) lahutamine
//   c) korrutamine, d)jagamine
//    prindi asutajale tehte vastus
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String answer = "";
        boolean wrongAnswer;


        do {
            System.out.println("Sisesta esimene number");


            int a = Integer.parseInt(scanner.nextLine());
            System.out.println("Sisesta teine number");
            int b = Integer.parseInt(scanner.nextLine());


            do {
                System.out.println("vali tehe");
                System.out.println("a) liitmine");
                System.out.println("b) lahutamine");
                System.out.println("c) korrutamine");
                System.out.println("d) jagamine");
                answer = scanner.nextLine(); // mida kasutaja vastas
                wrongAnswer = false;
                if (answer.equals("a")) {
                    System.out.printf("arvude %d ja %d summa on %d%n ", a,b, sum (a,b));

                    System.out.printf("summa on %d",sum(a,b));

                }
                else if (answer.equals("b")){
                    System.out.printf("arvude %d ja %d lahutus on %d%n ", a,b, sub (a,b));
                    System.out.printf("lahutus on %d",sub (a,b));

                }else if (answer.equals("c")) {
                    System.out.printf("arvude %d ja %d korrutus on %d%n ", a,b, multiply (a,b));
                    System.out.printf("korrutus on %d",multiply (a,b));

                }else if (answer.equals("d")) {
                    System.out.printf("arvude %d ja %d jagatis on %.2f%n ", a,b, divide (a,b));
                    System.out.printf("jagatis on %.2f",divide (a,b));

                }
                else {
                    System.out.println("Selline tehe puudub");
                    wrongAnswer = true;
                }
            } while (!answer.equals("a")&& !answer.equals("b")&& !answer.equals("c")&& !answer.equals("d"));
            while (wrongAnswer);

            System.out.println(" kas tahad veel arvutada?");

        } while (scanner.nextLine().equals("j"));

    }
    //    meetod, mis liidab kaks täisarvu kokku ja tagastab nende summa
    static int sum (int a, int b) {
        int sum = a + b;
        return sum;
    }
    //    Subract
    static int sub (int a, int b) {
        int sub = a - b;
        return sub;
    }
    //    Multiply
    static int multiply ( int a, int b){
        return a*b;
    }
    //    divide
    static double divide(int a, int b) {
        return (double)a / b;
    }

}
