package it.vali;

import java.sql.Driver;
import java.util.ArrayList;
import java.util.List;

enum Fuel {
    GAS, PETROL, DIESEL, HYBRID, ELECTRIC
}

public class Car {
    private String make;
    private String model;
    private int year;
    private Fuel fuel;
    private boolean isUsed;
    private Person driver;
    private Person owner;
    private int maxPassenger;
    //
    private List<Person> passengers = new ArrayList<Person>();


    private boolean isEngineRunning;
    private int speed;
    private int maxSpeed;


    //    Konstruktor Constructor
//    on eriline meetod, mis käivivtakse klassist objekti loomises
    public Car() {
        System.out.println("Loodi auto objekt");
        maxSpeed = 200;
        isUsed = true;
        isEngineRunning = false;//vaikimisi ongi false
        fuel = Fuel.PETROL;
        maxPassenger = 4;

    }

    //    Constructor overloading
    public Car(String make, String model, int year, int maxSpeed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;

    }

    public Car(String make, String model, int maxPassenger) {
        this.make = make;
        this.model = model;
        this.maxPassenger = maxPassenger;
    }

    public Car(String make, String model, int year, int maxSpeed, Fuel fuel, int speed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
        this.fuel = fuel;
        this.speed = speed;
    }

    public Car(Person driver, Person owner) {
        this.driver = driver;
        this.owner = owner;
    }


    public void startEngine() {
        if (!isEngineRunning) {
            isEngineRunning = true;
            System.out.println("Mootor käivitus");
        } else {
            System.out.println("Mootor juba töötab");
        }
    }

    public void stopEngine() {
        if (isEngineRunning) {
            isEngineRunning = false;
            System.out.println("Mootor seiskus");
        } else {
            System.out.println("Mootor ei töötanudki");
        }
    }

    public void accelerate(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa kiirendada");
        } else if (targetSpeed > maxSpeed) {
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Auto maksismum kiirus on %d%n", maxSpeed);

        } else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivne");
        } else if (targetSpeed < speed) {
            System.out.println("Auto ei saa kiirendada väiksemale kiirusele");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto kiirendas kuni kiiruseni %d%n", targetSpeed);
        }
    }

    //    slowDown (int targetSpeed)--speedi langus o'ni. mootor välja lülitamine
    public void slowDown(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa aeglustada");
        } else if (targetSpeed == 0) {
            System.out.println("Auto seisab, lülita välja mootor");
        } else if (targetSpeed > 0) {
            System.out.println("vajuta pidur");
        } else {
            speed = targetSpeed;
            System.out.printf("auto aeglustus kuni %d%n", targetSpeed);
        }

    }

    //    parkimine() mis tegevused oleks vajA teha (kutsu välja juba olemasolevaid meetodieid)
    public void parking() {
        slowDown(0);
        stopEngine();

    }

    public Person getDriver() {
        return driver;
    }

    public void setDriver(Person driver) {
        this.driver = driver;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public int getMaxPassenger() {
        return maxPassenger;
    }

    public void setMaxPassenger(int maxPassenger) {
        this.maxPassenger = maxPassenger;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void addPassenger(Person passenger) {
        if (passengers.size() < maxPassenger) {
            if(passengers.contains(passenger)){
                System.out.printf("See reisija on juba autos %s%n",passenger.getFirstName());

            }else { passengers.add(passenger);
                System.out.printf("autosse lisati reisija %s%n", passenger.getFirstName());
            }if (passengers.size() ==maxPassenger){
                System.out.println("autosse ei mahu enam reisijaid");
            }

        } else {


        }

    }

    public void removePassenger(Person passenger) {
        if (passengers.indexOf(passenger) != -1) {
            System.out.printf("autost emaldati reisija %s%n", passenger.getFirstName());
            passengers.remove(passenger);

        } else {
            System.out.println("Sellist reisijad autos ei ole");
        }
    }

    public void showPassengers() {
//        Foreach loop või Foreach tsükel-see tsükkel on listide läbimiseks
//        iga elemendi kohta listist passengers tekita objekt passenger
//        1. kordus Person passenger on esimene reisija
//        2: kordus Person passenger on teine reisija jne.
        System.out.println("Autos on järgnevad reisijad:");
        for (Person passenger : passengers) {
            System.out.println(passenger.getFirstName());

        }
    }


//    lisa autole parameetrid driver ja owner (tüübist Person) ja nendele siis get ja set meetodid
//    Loo mõni auto objekt, kellel on siis määratud kasutaja ja omanik

//    Prindi välja auto omaniku vanus


}
//Lisa autole max reisijate arv
//lisa autole võimalus hoida reisijaid
//lisa meetodid reisijate lisamiseks ja eemaldamiseks autost
//kontrolli ka, et ei lisaks rohkem reisijaid kui manub autosse
