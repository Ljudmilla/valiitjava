package it.vali;

import java.sql.Driver;
import java.util.concurrent.Callable;

public class Main {

    public static void main(String[] args) {
        Car bmw = new Car();
        bmw.startEngine();
        bmw.startEngine();
        bmw.stopEngine();
        bmw.stopEngine();
        bmw.accelerate(100);
        bmw.startEngine();
        bmw.accelerate(100);
        bmw.getMaxPassenger();

        Car fiat = new Car();
        Car mercedes = new Car();
        System.out.println();

        Car opel = new Car("Opel", "Vectra", 1999, 205);
        opel.startEngine();
        opel.accelerate(205);
        System.out.println();
        opel.slowDown(0);
        System.out.println();
        opel.parking();
        opel.setMaxPassenger(4);

        System.out.println();

        Car audi = new Car("Audi", "A6", 4);
        audi.getMaxPassenger();
        System.out.println("Auto " + audi.getMake() + " " + audi.getModel() + " mahub " + audi.getMaxPassenger() + " reisijad");


        Person person = new Person("Ljuda", "Vino", Gender.FEMALE, 18);
        Person driver = new Person("Mati", "Mets", Gender.MALE, 30);
        Person owner = new Person("Kaido", "Luik", Gender.MALE, 50);
        Person muuPerson = new Person("Sigrid", "Kask");
        System.out.println();
        Person person1 = new Person("Peeter", "Kask");
        System.out.println("nimi on " + person1.getFirstName());

        System.out.println();
//        ülesanne reisijatega
        opel.addPassenger(person);
        opel.addPassenger(owner);
        opel.addPassenger(owner);
        opel.showPassengers();
        opel.removePassenger(owner);
        opel.removePassenger(muuPerson);
        System.out.println();

        Car toyota = new Car(driver, owner);

        System.out.println("Auto omaniku vanus on " + toyota.getOwner().getAge());
        System.out.println("Auto juht on " + toyota.getDriver().getFirstName() + " ja ta on " + toyota.getDriver().getAge() + " aastat vana");

        Person juku = new Person("Juku", "Juurikas", Gender.MALE, 23);
        Person malle = new Person("Malle", "Maasikas",Gender.FEMALE, 30);
        System.out.println(juku);
        System.out.println(malle);
//        System.out.printf("Sõiduki %s omaniku vanus on %d%n",opel.getMake(),opel.getOwner().getAge());

    }
}
