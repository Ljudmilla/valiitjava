package it.vali;

enum Gender {
    FEMALE, MALE, NOTSPECIFIED
}

public class Person {
    private String firstName;
    private String lastName;



    private Gender gender;
    private int age;

//    kui klassil ei ole defineeritud konstruktorit, siis tegelikult tehakse
//    nähtamatu parameetrita konstruktor, mille sisu on tühi

    //    kui klassile ise lisada mingi konstruktor, siis see nähtamatu parameetrita kons-r kustutatakse
//    sellest klassist saab teha objekti ainult selle uue konstruktoriga
//    private Person() {
//
//    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }
// Konstruktor
    public Person(String firstName, String lastName, Gender gender, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
    }
//    Konstruktor
    public Person(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }
    @Override
    public String toString() {
        return String.format("Eesnimi on: %s, perekonna nimi on %s, vanus on %d",firstName, lastName, age) ;
    }
}
