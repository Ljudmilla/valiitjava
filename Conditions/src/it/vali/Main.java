package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta number");

        Scanner scanner = new Scanner(System.in);
        int a = Integer.parseInt(scanner.nextLine());

        // Java on type - safe language

        if (a == 3) {
            System.out.printf("Arv %d on võrdne kolmega%n", a);
        }
        if (a < 5) {
            System.out.printf("Arv %d on väiksem viiest%n", a);
            {
            }
            if (a != 4) {
                System.out.printf("Arv %d ei võrdu neljaga%n", a);
            }
            if (a > 2) {
                System.out.printf("Arv %d on suurem kahest%n", a);
            }
            if (a >= 7) {
                System.out.printf("Arv %d on suurem või võrdne seitsmegat%n", a);
            }
            if (!(a >= 7)) {
                System.out.printf("Arv %d oei ole suurem või võrdne seitsmegat%n", a);
            }
            //arv on 2 ja 8 vahel
            if (a > 2 && a < 8) {
                System.out.printf("Arv %d on 2 ja 8 vahel%n", a);
            }
            // Kui arv on väoiksem kui 2 ja suurem kui 8
            if (a < 2 || a > 8) {
                System.out.printf("Arv %d on väiksem kui 2 või arv on suurem kui 8 %n", a);

            }

            //kui arv on 2  ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10
            if ((a > 2 && a < 8) || (a > 5 && a < 8) || a > 10) {
                System.out.printf("Arv %d on 2 ja 8 vahel või arv on 5 ja 8 vahel või suurem kui 10 %n", a);
            }
            //kui arv ei ole 4  ja 6 vahel aga on 5 ja 8 vahel
            // või arv on negatiivne aga pole suurem kui -14
            if ((!(a > 4 && a < 6) && (a > 5 && a < 8)) || (a < 0 && !(a > -14))) {
                System.out.printf("Arv %d ei ole 4 ja 6 vahel, aga on 5 ja 8 vahel või on negatiivne, aga pole suurem kui -14%n", a);
            }


        }
    }

}







