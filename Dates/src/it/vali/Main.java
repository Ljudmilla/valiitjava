package it.vali;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;

public class Main {

    public static void main(String[] args) {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss EEEE");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        Date date = calendar.getTime();

//        System.out.println(dateFormat.format(date));
//        calendar.add(Calendar.YEAR, 1);
//        date = calendar.getTime();
//        System.out.println(dateFormat.format(date));
//        prindi ekraanile selle aasta järele jäänud kuude esimese kuupäeva nädalapäevad
        System.out.println(calendar.get(Calendar.MONTH));
//        selle aasta selle kuu esimene kuupäev
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);
        int monthsLeftThisYears = 11 - calendar.get(Calendar.MONTH);
        DateFormat weekdayFormat = new SimpleDateFormat("EEEE");
//        for tsükliga
//        for (int i = 0; i < monthsLeftThisYears; i++) {
//        calendar.add(Calendar.MONTH, 1);
//        date = calendar.getTime();
//        System.out.println(weekdayFormat.format(date));}
//    while tsükliga
        int currentYear = calendar.get(Calendar.YEAR);
        calendar.add(Calendar.MONTH, 1);
        while (calendar.get(Calendar.YEAR) == currentYear) {
            calendar.add(Calendar.MONTH, 1);
            date = calendar.getTime();
            System.out.println(weekdayFormat.format(date));

        }


    }
}

