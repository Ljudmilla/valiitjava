package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        Do While tsükkel on nagu while tsükkel,
//        ainult, et kontroll tehakse peale esimest korda

//	// tsükkel nagu while, kontroll tehakse peale tsükli
//        // jatkame seni kuni kasutaja kirjutab "ei"
//
//        System.out.println("kas tahad jätkata? jah/ei");
//
//
//        Scanner scanner  = new Scanner(System.in);
//
//        String answer = scanner.nextLine();
//
//        while (!answer.equals("ei")) {
//            System.out.println("kas tahad jätkata? jah/ei");
//            answer = scanner.nextLine();
//
//        }

        Scanner scanner  = new Scanner(System.in);
        String answer;
        // iga muutuja, mille me deklareerime kehtib ainult ümbritsevate {} sulgude sees!

        do {
            System.out.println("kas tahad jätkata? jah/ei");
            answer = scanner.nextLine();

        }

        while (!answer.equals("ei"));

    }






}


