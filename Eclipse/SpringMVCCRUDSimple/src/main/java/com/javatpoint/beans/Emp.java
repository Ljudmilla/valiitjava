package com.javatpoint.beans;  
  
public class Emp {  
	private int id;  
	private String first_name;  
	private String last_name;
	private String employee_type;
	private String employee_dpt; 
	private int employee_dur;
	  
	
	public int getId() {  
	    return id;  
	}  
	public void setId(int id) {  
	    this.id = id;  
	}  
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}  
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getEmployee_type() {
		return employee_type;
	}
	public void setEmployee_type(String employee_type) {
		this.employee_type = employee_type;
	}
	public String getEmployee_dpt() {
		return employee_dpt;
	}
	public void setEmployee_dpt(String employee_dpt) {
		this.employee_dpt = employee_dpt;
	}

	public int getEmployee_dur() {
		return employee_dur;
	}
	public void setEmployee_dur(int employee_dur) {
		this.employee_dur = employee_dur;
	}
	
	
	  
	}  