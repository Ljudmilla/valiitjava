package com.javatpoint.dao;  
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;  
import com.javatpoint.beans.Emp;  
  
public class EmpDao {  
	JdbcTemplate template;  
	  
	public void setTemplate(JdbcTemplate template) {  
	    this.template = template;  
	}  
	public int save(Emp p){  
	    String sql="insert into Employees(first_name,last_name, employee_type, employee_dpt, employee_dur)"
	    		+ " values('"+p.getFirst_name()+"',"+p.getLast_name()+",'"+p.getEmployee_type()+"','"+p.getEmployee_dpt()+"'"+p.getEmployee_dur()+")";  
	    return template.update(sql);  
	}  
  
	public int delete(int id){  
	    String sql="delete from Employees where id="+id+"";  
	    return template.update(sql);  
	}  
	public Emp getEmpById(int id){  
	    String sql="select * from Employees where id=?";  
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Emp>(Emp.class));  
	}  
	public List<Emp> getEmployees(){  
	    return template.query("select * from Employees",new RowMapper<Emp>(){  
	        public Emp mapRow(ResultSet rs, int row) throws SQLException {  
	            Emp e=new Emp();  
	            e.setId(rs.getInt(1));  
	            e.setFirst_name(rs.getString(2));  
	            e.setLast_name(rs.getString(3));  
	            e.setEmployee_type(rs.getString(4)); 
	            e.setEmployee_dpt(rs.getString(5));
	            e.setEmployee_dur(rs.getInt(5));
	            return e;  
	        }  
	    });  
	}
	
	
	
	}  