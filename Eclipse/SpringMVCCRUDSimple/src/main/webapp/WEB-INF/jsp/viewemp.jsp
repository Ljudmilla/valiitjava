    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

	<h1>Employees List</h1>
	<table border="2" width="70%" cellpadding="2">
	<tr><th>Id</th><th>FirstName</th><th>LastName</th><th>EmpType</th><th>EmpDpt</th><th>EmpDur</th><th>Balance</th><th>Edit</th><th>Delete</th></tr>
    <c:forEach var="emp" items="${list}"> 
    <tr>
    <td>${emp.id}</td>
    <td>${emp.first_name}</td>
    <td>${emp.last_name}</td>
    <td>${emp.employee_type}</td>
    <td>${emp.employee_dpt}</td>
    <td>${emp.employee_dur}</td>
    <td>${emp.vac_balance}</td>
    <td><a href="editemp/${emp.id}">Edit</a></td>
    <td><a href="deleteemp/${emp.id}">Delete</a></td>
    </tr>
    </c:forEach>
    </table>
    <br/>
    <a href="empform">Add New Employee</a>