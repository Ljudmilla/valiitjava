package it.vali;

public class Main {

    public static void main(String[] args) {
//        int vikeväärtus 0
//        boolen vaikeväärtus on false
//        double vaikeväärtus 0.0
//        float vaikeväärtus on 0.0f
//        String vaikeväärtus null
//        Objektidel (Monitor,FileWriter) vaikeväärtus on null
//        int [] arvud; vaikeväärtus on null
        Monitor firstMonitor = new Monitor();
        firstMonitor.setDiagonal(-1000);
        System.out.println(firstMonitor.getDiagonal());
        firstMonitor.setDiagonal(200);
        System.out.println(firstMonitor.getYear());
        firstMonitor.setManufacturer(null);
    }
}
