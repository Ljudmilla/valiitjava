package it.vali;

import java.time.Year;

//Enum on tüüp, kus saab defineerida erinevaid lõplikke valikuid
// Tegelikult salvestatakse ENUM alati int'na
enum Color {
    BLACK, WHITE, GREY
}

enum ScreenType {
    LCD, TFT, OLED, AMOLED
}

public class Monitor {

    private String manufacturer;
    private double diagonal;// tollides
    private Color color;
    private ScreenType screenType;
    private int year;


    public int getYear() {
        return year;
    }
    //    Ära luba seadistada monitori tootjaks, mille tootja on Huawei
//    kui keegi soovib seda tootja monitori tootjaks panna
//    pannakse hoopis tootjaks teks "tootja puudub"

    //    keela ka tühja tootja nime lisama "", null
    public void setManufacturer(String manufacturer) {
        if (manufacturer == null || manufacturer.equals("Huawei")
                || manufacturer.equals("")
        ) {
            this.manufacturer = "tootja puudub";

        } else {
            this.manufacturer = manufacturer;
        }
    }


    public double getDiagonal() {
        return diagonal;
    }

    public void setDiagonal(double diagonal) {
//        This tähistab seda konkreetse objekti
        if (diagonal < 0) {
            System.out.println("diagonaal ei saa olla negatiivne");
        } else if (diagonal > 100) {
            System.out.println("diagonaal ei saa olla suurem kui 100");
        } else {
            this.diagonal = diagonal;
        }
    }


    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    //        kui ekraani tüüp on sedistamata (null)
//    siis tagastada tüübiks LCD
    public ScreenType getScreenType() {
        if (screenType == null) {
            return ScreenType.LCD;
        }
        return screenType;
    }

    public void setScreenType(ScreenType screenType) {

        this.screenType = screenType;
    }

    public String getManufacturer() {
        return manufacturer;
    }
//    public void setManufacturer(String manufacturer) {
//        this.manufacturer = manufacturer;
//    }


    public void printInfo() {
        System.out.println();
        System.out.println("Monitori info:");
        System.out.printf("Tootja: %s%n", manufacturer);
        System.out.printf("Diagonaal: %.1f%n", diagonal);
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraani tüüp: %s%n", getScreenType());
        System.out.printf("Aasta: %d%n", year);
        System.out.println();
    }

    //    tee meetod mis tagastab ekraani diagonaali cm'tes
    public double getDiagonalToCm() {
        return diagonal * 2.54;
    }

}
