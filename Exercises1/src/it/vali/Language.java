package it.vali;

import java.util.List;

public class Language {
//    kaks muutujad: LanguageName ja countryName
//    nendel on meetodid

//    Kirjuta üle selle klassi meetod toString() nii, et see tagastab
//    riikide nimekirja eraldades komaga. Selleks on Code override

    private String languageName;

    private List<String> countryNames;

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public List<String> getCountryNames() {
        return countryNames;
    }

    public void setCountryNames(List<String> countryNames) {
        this.countryNames = countryNames;
    }

    @Override
    public String toString() {
        return String.join(" , ", countryNames);
    }
}
