package it.vali;

public class Main {

    public static void main(String[] args) {
	// For tsükkel on selline tsükkel kus korduste arv on teada.

        //Lõpmatu for tsükkel
//        for ( ; ; ) {
//            System.out.println("Väljas on ilus ilm");
//        }
          // 1) int i = 0
          // siin saab luua muutujaid ja neid algväärtustada
          // luuakse täisarv I mille väärtus hakkab tsükli sees muutuma
          // 2) i < 3 => tingimus, mis peab olema tõene, et tsükel käivituks ja korduks
          // 3) i++  see on tegevus mida iga tsükli korduse lõpus korratakse
          // i++ on sama kui kirjutada i=i+1
          // i-- sama i=i-1


        for(int i = 0; i < 3; i++) {
            // i++ i suureneb ühe võrra
            System.out.println("Väljas on ilus ilm");

        }
        //Prindi ekraanile  numbrid 1 kuni 10
        for (int i = 1; i < 11 ; i++) {
            System.out.println(i);

        }
        System.out.println();
        for (int i = 100; i <= 110 ; i++) {
            System.out.println(i);
        }
        // 24kuni 167
        // 18 kuni 3
        System.out.println();
        for (int i = 24; i < 168; i++) {
            System.out.println(i);

        }
        System.out.println();
        for (int i = 18; i >= 3 ; i--) {
            System.out.println(i);

        }
        System.out.println();
        //Prindi ekraanile numbrid 2 4 6 8 10

        // i=i+2 => i+=2
        //  i = i-4 => i -=4
        // i = i * 3 => i *=3 arvu iseenda korrutamine MINGI NUMBRIGA
        // i = i / 2 => i /=2
        for (int i = 2; i < 11 ; i=i+2) {
            System.out.println(i);
        }
        System.out.println();
        // prindi numbrid 10 kuni 20 ja 40 kuni 60

        for (int i = 10; i < 61; i++) {
            if (i <= 20 || i >= 40)
                System.out.println(i);
            // if (i == 21) {
            // i = 40;
        //}

        }
        System.out.println();
        // prindi kõik arvud, mis jaguvad 3'ga vahemikus 10 uni 50
        // a % 3
        // 4 % 3 => jääk 1
        // 6 % 3 => jääk 0
        // if (a % 3 == 0)
        for (int i = 10; i <=50 ; i++) {
            if (i % 3 == 0)
                System.out.println(i);
        }

        // prindi paaritu numbrid
        System.out.println();
        for (int i = 10; i <=50 ; i++) {
            if (i % 2 != 0)
                System.out.println(i);
        }


    }
}
