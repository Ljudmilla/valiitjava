
package it.vali;

// import tähendab, et antud klassile Main lisatakse ligipääs java class libriary paketile
// java.util paiknevale klassile Scanner
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // loome Scanner tüübist objekti nimega scanner, mille kaudu saab kasutaja sisendit
        // lugeda

        Scanner scanner = new Scanner(System.in);

        System.out.println("Tere, mis su nimi on?");
        String name = scanner.nextLine();


        System.out.println("Tere, mis su auto on?");
        String car = scanner.nextLine();

        System.out.println("Tere, mis su töö on?");
        String job = scanner.nextLine();

        System.out.println("Tere, " + name + " nimi on meeldiv " + car + " on hea auto " + job + " töö on äge\n");

        System.out.printf("Tere %s nimi on meeldiv %s on hea auto %s töö on äge\n",
        name, car, job);

        StringBuilder builder = new StringBuilder ();
        builder.append("Tere ");
        builder.append(name);
        builder.append(" nimi on meeldiv ");
        builder.append(car);
        builder.append(" on hea auto ");
        builder.append(job);
        builder.append(" töö on äge");
        String fullText = builder.toString();

        System.out.println(fullText);

        // nii System.out.printf kui ka  String.formatkasutavad enda siseselt  StringBuiderit

        String text = String.format("Tere %s nimi on meeldiv %s on hea auto %s töö on äge\n",
                name, car, job);
        System.out.println(text);







    }
}
