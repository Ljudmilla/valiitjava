package it.vali;
// public - tähendab, et klass, meetod või muutuja on avalikult nähtav/ligipääsetav
//
// class - javas üksus, üldiselt on ka eraldi fail, mis sisaldab/grupeerib mingit funktsionaalsust
// static - meetodi ees tähendav, et seda meetodit saab välja kutsuda ilma klassist objekti loomata
// HelloWorld - klassi nimi, mis on ka faili nimi
// void - meetod ei tagasta midagi. Meetodile on võimalik anda parameetrid, mis pannakse sulgude sisse, eraldadades komaga
// String[] - tähistatakse stringi massivi
// args - massiivi nimi, sisaldav käsurealt kaasa pandud parameetreid
// System.out.println -on java meetod, millega saab printida välja rida teksti
//              See mis kirjutatakse sulgudesse prinditakse välja ja tehakse reavahetus


public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");

    }
}
