package it.vali;

public class Main {

    public static void main(String[] args) {
//   Muutuja deklareerida ja anda väärtust saab ka kahe sammuga

        String word;
        word = "kala";

        int number;
        number = 3;

        int secondNumber = -7;

        System.out.println(number);
        System.out.println(secondNumber);
        System.out.println(number + secondNumber);
        System.out.println(number * secondNumber);

        //Arvude 3 ja -7 korrutus on -21
        // %n tekitab platvorminr-spetsiifilise reavahetuse (Windows\r\n ja Linux/Mac \n)
         //
        // selle asemel saab kasuada ka System.lineSeparator versioon
        int product = number * secondNumber;
        System.out.printf("Arvude %d ja %d korrutus on %d%n", number, secondNumber, product);

        System.out.printf("Arvude %d ja %d korrutus on %d", number, secondNumber, number * secondNumber);

        System.out.printf("Arvude %d ja %d korrutus on %d%s", number, secondNumber, product, System.lineSeparator());

        int a = 3;
        int b = 14;
        // String + number on alati string
        // Stringi liitmisel numbriga, teisendatakse number stringiks ja liidetakse kui liitsõna
        System.out.println("Arvude summa on " + (a + b));
        System.out.println("Arvude summa on " + a + b);

        System.out.printf("Arvude %d ja %d summa on %d%n", number, secondNumber, number + secondNumber);

        System.out.printf("Arvude %d ja %d jagatis on %d%n", number, secondNumber, number / secondNumber);

        System.out.println("Arvude jagatis on " + (b / a));
        System.out.printf("Arvude %d ja %d jagatis on %d%n", b, a, b / a);

//     Kahe täisarvu jagamisel on tulemuse jagatise täisosa ehk kõik peale koma süüakse ära

        int maxInt = 2147483647;
        int c = maxInt + 1;
        int d = maxInt + 2;
        int e = maxInt + maxInt;
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);

        int minInt = -2147483648;
        int r = minInt -1;
        System.out.println(r);

        Short f = 32199;
        // Long väärtust andes peab numbrile L tähe lõppu lisama
        long g = 999999999999L;
        long h = 234;
        System.out.println(g + h);















//
	// write your code here
    }
}
