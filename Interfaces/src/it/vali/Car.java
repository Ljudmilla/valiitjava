package it.vali;

public class Car extends Vehicle implements Driver{
    private int MaxDistance ;
    private String make;
    public Car(){
        this.MaxDistance=600;
    }

    private void startEngine() {
        System.out.println("Mootor käivitus");

    }

    @Override
    public void drive() {
        System.out.println("Auto sõidab");

    }

    @Override
    public int getMaxDistance() {
        return MaxDistance;
    }

    @Override
    public void stopDriving(int afterDistancein) {
        System.out.printf("Auto lõpetab sõitmise %d km pärast%n", afterDistancein);

    }
//toStringiga saab edastada seda mis meetodis on üle kirjutatud
    @Override
    public String toString() {
        return make;
//        return "Auto andmed: "+ make + " " + maxDistance;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }
}
