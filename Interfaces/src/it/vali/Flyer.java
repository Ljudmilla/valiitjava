package it.vali;

//Interface ehk liides sunnib seda kasutama/implementeerima klassi omama liidedes kirja pandud meetodeid
// ( sama tagastuse tüübiga ja sama parameetrite kombinatsiooniga)

// Iga klass, mis interface kasutab määrab ise ära meetodi sisu

//Pärimisel pärib kõik sisuga omadused ja classi sisu; Interfeisis kasutab meetod, aga sisu paneme ise.

public interface Flyer {
    void fly ();
}
