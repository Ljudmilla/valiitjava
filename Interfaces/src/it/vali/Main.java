package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Flyer bird = new Bird();
        Flyer plane = new Plane();

        bird.fly();
        System.out.println();
        plane.fly();

//        on võimalik hoida erinevad objektid ühes nimekirjas
        List<Flyer> flyers = new ArrayList<Flyer>();
        flyers.add(bird);
        flyers.add(plane);

        Plane boeing = new Plane();
        Bird pigeon = new Bird();

        flyers.add(boeing);
        flyers.add(pigeon);



        for (Flyer flyer : flyers) {
            flyer.fly();
            System.out.println();

        }
        Car audi = new Car();
        audi.stopDriving(100);
        audi.drive();
        audi.toString();
        audi.setMake("AudiA6");
        System.out.println(audi);
        System.out.println("tere".toString());
//      Olemas objekt ja ütlen, et prindi välja. Aga ei ütle konkreetselt mida ma prindin välja, siis prindib vaikimisi unikaalse koodi
        System.out.println(boeing);



    }
}

//Lisa liides Driver , klass Car . Mõtle, kas lennuk ja auto  võiks mõlemad kasutada Driver liidest?
// Driver liides võiks sisaldada 3 meetodi kirjeldust:
// int getMaxDistance ()
// void drive()
// void stopDriving(int afterDistance)-saab määrata, mitme meetri pärast lõpetab
// Pane auto ja lennuk mõlemad kasutama liidest
// Lisa mootorratas
