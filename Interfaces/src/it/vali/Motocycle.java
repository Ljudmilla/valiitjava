package it.vali;

public class Motocycle extends Vehicle implements DriverInTwoWheels {
    @Override
    public void drive() {
        System.out.println("Mootorratas sõidab");

    }

    @Override
    public int getMaxDistance() {
        return 300;
    }

    @Override
    public void stopDriving(int afterDistancein) {
        System.out.printf("Mootorattas lõpetab sõitmise %d km pärast%n", afterDistancein);

    }

    @Override
    public void driveInRearWheel() {
        System.out.println("Mootorratas sõidab tagarattal");

    }
}
