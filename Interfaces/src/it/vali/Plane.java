package it.vali;

//Plane pärineb klassist Sõiduk/Vehicle ja kasutab /impleminteerib liidest Flyer

public class Plane extends Vehicle implements Flyer, Driver  {
    private int maxDistance;
    @Override
    public void fly() {
        doChecklist();
        startEngine();

        System.out.println("Lennuk lendab");
    }

    private void doChecklist() {
        System.out.println("Tehakse checklist");
    }

    private void startEngine() {
        System.out.println("Mootor käivitus");

    }
    public void drive(){
        System.out.println("Lennuk sõidab");

    }

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void stopDriving(int afterDistancein) {
        System.out.printf("Lennul lõpetab sõitmise %d km pärast%n", afterDistancein);

    }

}