package it.vali;

public class Main {

    public static void main(String[] args) {
        Cat angora = new Cat();
        angora.setName("Musi");
        angora.setBreed("angora");
        angora.setAge(10);
        angora.setWeight(2.34);
        angora.printInfo();
        angora.eat();

        System.out.println();
        Cat persian = new Cat();
        persian.setName("Liisu");
        persian.setBreed("Persian");
        persian.setAge(1);
        persian.setWeight(3.11);
        persian.printInfo();
        persian.eat();




        System.out.println();

        Dog tax = new Dog();
        tax.setName("Mikki");
        tax.setBreed("Tax");
        tax.setAge(3);
        tax.setWeight(3.22);
        tax.printInfo();
        tax.eat();
        persian.catchMouse();
        tax.playWithCat(persian);

        System.out.println();

        Pet cat= new Pet();
        cat.askOwnerName();



    }
}
// Lisa pärinevusahelast puuduvad klassid
// Igale klassile lisa 1 muutuja ja 1 meetod,
// mis on ainult sellele klassile omane