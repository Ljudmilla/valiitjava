package it.vali;

public class Pig extends FarmAnimal {
private int fatPersent;

    public int getFatPersent() {
        return fatPersent;
    }

    public void setFatPersent(int fatPersent) {
        this.fatPersent = fatPersent;
    }
    public void fatPersent(){
        System.out.printf("Põrsas on %s%n protsent rasva", fatPersent);
    }

}
