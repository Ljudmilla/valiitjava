package it.vali;

public class CarnivoreAnimal extends WildAnimal {
    private boolean liveInWater = true;

    public boolean isLiveInWater() {
        return liveInWater;
    }

    public void setLiveInWater(boolean liveInWater) {
        this.liveInWater = liveInWater;
    }
    public void swims(){
        System.out.println("Oskab ujuda");
    }
}
