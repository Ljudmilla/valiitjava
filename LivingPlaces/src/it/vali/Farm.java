package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Farm implements LivingPlace {

//    Kui tahame Farmi panema elama loomi

//     List in interfeist ja new ArrayList on object

//    tegime classile privaatmuutuja List

    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>();

    //    Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();
    //    Siin hoitakse infot paju meil igat looma farmi mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Farm() {
//        kui palju mahub loomi
        maxAnimalCounts.put("Pig", 1);
        maxAnimalCounts.put("Cow", 3);
        maxAnimalCounts.put("Sheep", 15);
    }

    //    palju võiks loomi lisada?
    public void addAnimal(Animal animal) {
//        kas animal on tüübist FarmAnimal või pärib sellest tüübist?
//        kui loom ei ole Farm Animalst grupist: !FarmAnimal.class.isInstance(animal
        if (!FarmAnimal.class.isInstance(animal)) {
            System.out.println("Farmis saavad elada ainult farmi loomad");
            return;
        }
//        saab küsida classi nime (looma nimi)
//        küsime objekti classi ja oma korda nime
        String animalType = animal.getClass().getSimpleName();


        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Farmis saavad elada ainult Farmi loomad, sellist looma ei saa olla");
            return;
        }


        //vaatame kas see Key on olemas?
        if (animalCounts.containsKey(animalType)) {
            //        teeme abimuutujad
            int maxAnimalCount = maxAnimalCounts.get(animalType);//vaatame kui palju meil sigu
            int animalCount = animalCounts.get(animalType);
            if (animalCount >= maxAnimalCount) {
                System.out.println("Farmis on sellele loomale kõik kohad juba täis");
                return;
            }

            animalCounts.put(animalType, animalCounts.get(animalType) + 1);

//             kui else ploki läheb, siis sellist looma veel ei ole farmis,
//             siis kindlasti sellele loomale kohta on
        } else {

            animalCounts.put(animalType, 1);
        }
        animals.add((FarmAnimal) animal);// toimub looma lisamine
        System.out.printf("farmi lisati loom %s%n", animalType);


    }

    //    tee meetod, mis prindib välja kõik farmis elavad loomad ja mitu neis on
    public void printAnimalCounts() {
        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }

    //    tee meetod, mis eemaldab farmist looma
    public void removeAnimal(String animalType) {
//        kuidas leida nimekirjast loom kelle nimi on Kalle
        boolean animalFound = false;
        for (FarmAnimal animal : animals) {
//         IFiga   leiame esimese looma
            if (animal.getClass().getSimpleName().equals(animalType)) {
//                animals.remove(farmAnimal);
                System.out.printf("farmist eemaldati loom %s%n", animalType);
//                kui se oli viimane loom, siis eemalda see rida animalCounts mapist
//                muul juhul vähenda animalCounts mapis seda kogus

//             animalCounts.get(animalType)---tagastab INT'i
                if (animalCounts.get(animalType) == 1) {
                    animalCounts.remove(animalType);

                } else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }
                animalFound = true;
                break;

            }

        }
        if (!animalFound) {
            System.out.println("Farmis antud loom puudub");
        }
//        teen muutuja , mis ma tahan , et iga element listist oleks: list mida tahan läbi käia
//        FarmiAnimal animal hakkab olema järjest esimene loom,
//        siis teine loom, kolmas ja seni kuni loomi on
        for (int i = 0; i < animals.size(); i++) {
            if (animals.get(i).getClass().getSimpleName().equals(animalType)) {
                animals.remove(animals.get(i));
            }

        }

//täienda meetodit nii, et kui ei leitud ühtegi sellest tüübist looma
// prindi "Farmis selline loom puudub"
    }


}
