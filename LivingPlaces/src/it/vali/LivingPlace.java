package it.vali;

public interface LivingPlace {
    void addAnimal(Animal animal);
    void printAnimalCounts();
    void removeAnimal(String animalType);
}
//Exception-ide jaoks on võimalik kasutada Interfeisi--kasutaja ei pea teadma exceptionidest
//kui kaks arendajad kirjutavad koodi, siis nad võivad kasutada sama interfeisi, ehk muutujad; lepitakse interfeisid kokku.