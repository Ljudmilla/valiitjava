package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Zoo implements LivingPlace {

    private ArrayList<Animal> animals = new ArrayList<>();
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Zoo (){
        maxAnimalCounts.put("Lion", 1);
        maxAnimalCounts.put("Fox", 3);
    }

    @Override
    public void addAnimal(Animal animal) {
        animals.add(animal);
        if (Pet.class.isInstance(animal)){
            System.out.println("Lemmiklooma loomaaeda vastu ei võeta");
                    return;
        }
        String animalType = animal.getClass().getSimpleName();
        System.out.printf("Zoo lisati loom %s%n",animal.getClass().getSimpleName());

    }

    @Override
    public void printAnimalCounts() {
      int size = animals.size();
      System.out.println(size);

    }

    @Override
    public void removeAnimal(String animalType) {
        for (Animal animal: animals){
            if (animal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(animal);
                System.out.printf("Zoo-st eemaldati loom %s%n", animalType);}
        }


    }
}
