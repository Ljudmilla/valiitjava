package it.vali;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
//        Sõnaraamat
//        key => value
//        Isa => Dad
//        Maja => House jne
//        Mapis saab hoida paarid

        Map<String, String> map = new HashMap<String, String>();
        map.put("Maja", "House");
        map.put("Isa", "Dad");
        map.put("Puu", "Tree");
        map.put("Sinine", "Blue");
// // Map järjekorda ei salvesta!!
//        Foreach tsükkel küsib mis on sõnaraamatus:
        for (Map.Entry<String, String> entry : map.entrySet()
        ) {
            System.out.println(entry.getKey() + " " + entry.getValue());

        }
        System.out.println();
//        Oletame et tahad teada mis on inglise keeles on puu

        String translation = map.get("Puu");
        System.out.println(translation);

        Map<String, String> idNumberName = new HashMap<String, String>();
//        isikukoode hoiatakse stringina
        idNumberName.put("38510250305", "Lauri");
        idNumberName.put("38712050224", "Malle");
        idNumberName.put("38712050224", "Kalle");

//        kui kasutada put sama key lisaminel, kirjutatakse value üle
//        Key on alati unikaalne
//
        System.out.println(idNumberName.get("38712050224"));
//        kustutada saab removega

        idNumberName.remove("38712050224");
        System.out.println(idNumberName.get("38712050224"));

//        EST => Estonia
//        Estonia => +372

//        Loe lauses üle kõik erinevad tähed ning prindi välja
//        iga tähe järel, mitu tükki teda selles lauses oli
//        Char on selline tüüp, kus saab hoida üksikut sümbolit
        char symbolA = 'a';
        char symbolB = 'b';
        char newLine = '\n';
//
//        Mingi asja kokku lugemine

        String sentence = "elas metsas mutionu";
        Map<Character, Integer> letterCounts = new HashMap<Character, Integer>();
//        stringil on meetod mis tagastab massiivi
        char[] characters = sentence.toCharArray();
        for (int i = 0; i < characters.length; i++) {
//            kontrollime kas täht on olemas
            if (letterCounts.containsKey(characters[i])) {
//                kui leitud see mis on olemas, siis paneme phe juurde ja asendan
                letterCounts.put(characters[i], letterCounts.get(characters[i]) + 1);

            } else {
//                kui esimese leian siis panen numbri 1
//                küsin palju neid oli letterCounts.get(characters[i]) abil
                letterCounts.put(characters[i], 1);
            }
            System.out.println(characters[i]);

        }
//    Map.Entry<Character, Integer> on eraldi class, mis hoiab endas ühte rida map-ist
//        ehk ühte key-value paari.
//        Map.Entry<Character, Integer> entry on klass nimega Entry;
//        selleks et Entry kätte saada on meetod entrySet())

        System.out.println(letterCounts);
        for (Map.Entry<Character, Integer> entry : letterCounts.entrySet()) {
            System.out.printf("Tähte %s esines %d korda%n", entry.getKey(), entry.getValue());

        }

//        e 2
//        l 1
//        a 2

//Kui tahame et säiliks sisamise järjekord siis kasutame LinkedhasMapi
        Map<String, String> linkedmap = new LinkedHashMap<>();
        linkedmap.put("Maja", "House");
        linkedmap.put("Isa", "Dad");
        linkedmap.put("Puu", "Tree");
        linkedmap.put("Sinine", "Blue");
// // Map järjekorda ei salvesta!!
//        Foreach tsükkel küsib mis on sõnaraamatus:
        for (Map.Entry<String, String> entry : linkedmap.entrySet()
        ) {
            System.out.println(entry.getKey() + " " + entry.getValue());

        }
        System.out.println();
    }
}
