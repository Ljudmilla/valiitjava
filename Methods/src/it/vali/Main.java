package it.vali;

public class Main {
    //	  Meetodid on mingid koodi osad, mis grupperivad mingid teatud kindlat funktsionaalsust
    //    Kui koodis on korduvaid koodi osasi, võiks mõelda, et äkki peaks nende kohta tegema eraldi meetodi

    public static void main(String[] args) {
//        System.out.println("Hello");
//        System.out.println("Hello");
//        System.out.println("Hello");
//        System.out.println("Hello");
//        System.out.println("Hello");
        printHello();
        printHello(3);
        printHello(2);
        printText("Kuidas läheb?");
        printText("hästi läheb", 6);
        printText("2", 2);
        printText(2, "2");
        printText("tere", 1, true);
        printText("tere", 1, false);


    }

    //    Lisame meetodi, mis prindib ekraanile Hello
    private static void printHello() {
        System.out.println("Hello");
    }
    //    Lisame meetodi, mis prindib Hello etteantud arv kordi
//    privat static ja lihtsalt static on võrdsad
    static void printHello(int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println("Hello");
        }

    }
//    Lisame meetodi, mis prindib ette antud teksti välja
//    printText
    static void printText(String text) {
        System.out.println(text);
    }
//    Lisame meetodi, mis prindib ette antud teksti välja ette antud arv kordi
    static void printText(String text, int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++){
            System.out.println(text);

        }

    }
    //        Mõiste: Method OVERLOADING-meil on mitu meetodit sama nimega, aga erineva parameetrite kombinatsiooniga
//        Meetodi ülelaadimine
//    mis prindib ette antud teksti välja ette antud arv kordi.
//    lisaks saab öelda, kas tahame teha kõik tähed enne suureks või mitte

    static void printText(int year, String text) {
        System.out.printf("%d: %s%n", year, text);

    }
    static void printText(String text, int howManyTimes, boolean toUpperCase ) {
        for (int i = 0; i < howManyTimes; i++) {
            if(toUpperCase){
                System.out.printf(text.toUpperCase());

            }
            else {
                System.out.printf(text);

            }

        }
    }



}
