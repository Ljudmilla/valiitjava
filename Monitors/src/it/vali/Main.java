package it.vali;

public class Main {

    public static void main(String[] args) {
        Monitor firstMonitor = new Monitor();
        Monitor secondMonitor = new Monitor();
        Monitor thirdMonitor = new Monitor();

        firstMonitor.manufacturer = "Philips";
        firstMonitor.color = Color.WHITE;
        firstMonitor.diagonal = 27;
        firstMonitor.screenType = ScreenType.AMOLED;
        secondMonitor.manufacturer = "LG";
        secondMonitor.color = Color.WHITE;
        secondMonitor.diagonal = 25;
        secondMonitor.screenType = ScreenType.LCD;
        thirdMonitor.manufacturer = "Sony";
        thirdMonitor.color = Color.GREY;
        thirdMonitor.diagonal = 32;
        thirdMonitor.screenType = ScreenType.OLED;


        System.out.println(firstMonitor.manufacturer);
        System.out.println(secondMonitor.color);
        System.out.println(secondMonitor.screenType);

        firstMonitor.color = Color.BLACK;
//        teeme massiivi monitoridest
        Monitor[] monitors = new Monitor[4];
//        lisa massiivi 3 monitori
        monitors[0] = firstMonitor;
        monitors[1] = secondMonitor;
        monitors[2] = thirdMonitor;


        monitors[3] = new Monitor();
        monitors[3].color = Color.GREY;
        monitors[3].manufacturer = "Samsung";
        monitors[3].diagonal = 21;
        monitors[3].screenType = ScreenType.TFT;
//        Prindi välja kõigi monitooride tootja,
//        mille diagonaal on suurem kui 25 tolli
        for (int i = 0; i < monitors.length; i++) {
            if (monitors[i].diagonal > 25) {
                System.out.println(monitors[i].manufacturer);
            }

        }
        System.out.println();
//        leia monitori värv kõige suuremal monitoril
        Monitor maxSizeMonitor = monitors[0];

        for (int i = 0; i < monitors.length; i++) {
            if (monitors[i].diagonal > maxSizeMonitor.diagonal) {
                maxSizeMonitor = monitors[i];

            }
//            System.out.println(maxSizeMonitor.color);
//            System.out.println(maxSizeMonitor.manufacturer);
//            System.out.println(maxSizeMonitor.screenType);
//            System.out.println();
        }
        maxSizeMonitor.printInfo();
        firstMonitor.printInfo();
        System.out.printf("Monitori diagonaal cm'tes on %.2f%n",maxSizeMonitor.getDiagonalToCm());
        System.out.println();
//        prindib eraldi välja
        System.out.println(maxSizeMonitor.color);
        System.out.println(maxSizeMonitor.manufacturer);
        System.out.println(maxSizeMonitor.screenType);


    }
}
