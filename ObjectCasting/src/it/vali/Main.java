package it.vali;

import javax.swing.text.html.ListView;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        int a = 100;
        short b = (short) a;// b ei mahu a sisse
        a = b; // a mahus b sisse


        // Iga kassi või võtta kui looma
        Animal animal = new Cat();
        //Iga loom ei ole kass
        //explicite casting
//  Saab teha listi ja panna sinna kõik erinevad loomad

        Cat cat = (Cat) animal;
        List<Animal> animals = new ArrayList<Animal>();

        Dog dog = new Dog();
        dog.setName("Nuki");
        Pig pig = new Pig();
        Fox fox = new Fox();
        fox.setBreed("Hõberebane");


        animals.add(dog);
        animals.get(animals.indexOf(dog)).setName ("Peeter");
        dog.setName("Muki");
        animals.add(pig);
        animals.add(new Cow());
        animals.add(new Pet()); //saab panna kõik objekti mis pärinevad Animalist
        animals.add(new WildAnimal());
        animals.get(3).setName("Kuku");

        //    Kutsu kõikide listis olevate loomade printInfo  välja

        for (Animal animalInList : animals) {
            animalInList.printInfo();
            System.out.println();

        }
        for (Animal animalInList : animals) {
            animalInList.eat();
            System.out.println();

        }
        Animal secondAnimal = new Dog();
//        jaavad alles ainult Animali meetodid
//        peab tagastama , ehk teisendama tagasi koeraks
        ((Dog)secondAnimal).setHasTail(true);



    }
}
