package it.vali;

public class Cat extends Pet{


    private boolean hasFur = true;



    public void catchMouse(){
        System.out.println("Püüdsin hiire kinni");
    }

    public boolean isHasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }

    @Override
    public void eat (){
        System.out.println("Söön hiiri");

//"super" saab otsida lähima nö sugulase ja nii kaua kui ta leiab prindimise meetodi

    }
    @Override
    public void printInfo(){
        super.printInfo ();
        System.out.printf("Karvade olemasolu: %s%n",hasFur);

    }


}
