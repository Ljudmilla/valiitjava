package it.vali;

public class Dog extends Animal {


    private boolean hasTail = true;


    public void playWithCat(Cat cat) {
        System.out.printf("Mängin kassiga %s%n", cat.getName());
    }

    public boolean isHasTail() {
        return hasTail;
    }

    public void setHasTail(boolean hasTail) {
        this.hasTail = hasTail;
    }

    @Override
    public void eat() {
        System.out.println("Närin konti");

    }

    @Override
    public int getAge() {
//kutsume välja Animali getAge, vaatame mis seal sees on ja overridime seda
        int age = super.getAge();
        if ((age == 0)) {
            return 1;
        }
        return age;

    }
//    protected kasutamine : protected double weight Animal classis

//    public double getWeight() {
//        if(weight==0){
//            return 1;
//        }
//
//        return weight;
//    }
}
