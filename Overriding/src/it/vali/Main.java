package it.vali;

public class Main {

    public static void main(String[] args) {
// Meetodi Overriding ehk meetodi ülekirjutamine tähendab seda et kuskil klassis,
// millest antud klass pärineb, oleva meetodi sisu kirjutatakse pärinevas klassis üle, teise sõnaga:
//        päritava klassi meetodi sisu kirjutatakse pärinevas klassis üle

        Dog buldog = new Dog();
        buldog.eat();
        buldog.printInfo();
        System.out.println();

        Cat siam = new Cat();
        siam.eat();
        siam.setOwnerName("Kalle");
        siam.printInfo();

        System.out.println();
        System.out.println(buldog.getAge());
        System.out.println(siam.getAge());
//        WildAnimal wildanimal= new WildAnimal();
//        wildanimal.printInfo();


    }


//    Kirjuta koera getAge üle nii et kui koeral vanus on 0, siis näitaks ikka 1
//    Kirjuta metsloomadel printinfo võiks kirjutada Nimi: metsloomal pole nime


}