package it.vali;

public class Pet extends DomesticAnimal {
    private String ownerName;

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void askOwnerName() {
        System.out.printf("Omaniku nimi on %s%n", ownerName);
    }
    public void printInfo(){
        super.printInfo();
        System.out.printf("omaniku nimi on %s%n", ownerName);
    }

}
