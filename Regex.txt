[0-9] {1} -tegu on ühe numbriga 0 ja 9 vahel
[0-9] {2} -tegu on kahe numbriga 0 ja 9 vahel

Matchib sisendi osa. Tähendab, et sisend peab sisaldama sellist regexi

^[0-9]{4}$
^ alguse sümbol
$ lõpu sümbol
Matchib ainult 4 kohalist numbrit

^[0-9]{5,10}$
Matchib 5 kuni 10 kohalist numbrit

[0-9]+
Matchib 1 kuni lõpmatu kohalised numbrid

[a-zöäüõ]{1,10}
Matchib 1 kuni kümne tähelised tähed

[a-zA-Z ÜÕÖÄöäüõ]+
Matchib kõik sõnad nii suurte kui väikeste algustähtedega

.? küsimärk tähendab, et eelnev kas on või ei ole

[a-z0-9\._]{1,50}@[a-z0-9]{1,50}\.[a-z]{2,10}\.?[a-z]{1,2}
Valideerib, kas on e mail või mitte

([a-z0-9\._]{1,50})@[a-z0-9]{1,50}\.([a-z]{2,10})\.?([a-z]{1,2})?
Sulgudega saab välja võtta meid huvitavad osad

(^\+?[0-9]{3} )?([0-9 ]{3,9})
5301013
+372 5301013
372 5301013
372 530 10 13

[^ ]+
^kandiliste sulgude sees tähistab eitust
Üks kuni mitu mitte-tühikut

[0-9\-]+ [0-9:,]+ \([a-z0-9\-\.]+\) \[[a-z0-9A-Z:]*\]-->tulemused samad
\] (.+)
2015-08-19 00:00:12,527 (http--0.0.0.0-28080-14) [CUST:CUS85J3381] /chekSession.do in 127
