package it.vali;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta email");
        Scanner scanner = new Scanner(System.in);

        String email = scanner.nextLine();
        String regex = "([a-z0-9\\._]{1,50})@[a-z0-9]{1,50}\\.([a-z]{2,10})\\.?([a-z]{1,2})?";
        if (email.matches(regex)) {
            System.out.println("email korrektne");
        } else {
            System.out.println("email ei ole korrektne");
        }

        Matcher matcher = Pattern.compile(regex).matcher(email);
        if (matcher.matches()) {
            System.out.println("kogu email: " + matcher.group(0));
            System.out.println("tekst vasakupool @ märki: " + matcher.group(1));
            System.out.println("domeeni laiend: " + matcher.group(2));

        }
        System.out.println("Sisesta isikukood");
        String personalId = scanner.nextLine();
        String regexID = "([1-6]{1})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{4})";

        if (personalId.matches(regexID)) {

        } else {
            System.out.println("Isikukood ei ole korrektne");
        }
        Matcher matcherID = Pattern.compile(regexID).matcher(personalId);

        if (matcherID.matches()) {
            System.out.println("Isikukood on " + matcherID.group(0));
            System.out.println("Sugukood " + matcherID.group(1));
            System.out.println("Sünniaasta " + matcherID.group(2));
            System.out.println("Sünnikuu " + matcherID.group(3));
            System.out.println("Sünnipäev " + matcherID.group(4));
            System.out.println("Sinu sünnipäev on " + matcherID.group(4) + "." + matcherID.group(3)
                    + "." + matcherID.group(2));

        }

    }
//    Küsi kasutajalt isikukood ja prindi välja tema sünnipäev
//    Mõelge ise mis piirangud isikukoodis peaks olema
//    aasta peab olema reaalne aasta, kuu number, kuupäeva number
//    ja prindi välja
}
