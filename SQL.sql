﻿--See on lihtne hello world teksti päring mis tagastab ühe rea ja ühe veeru 
-- (veerul puudub pealkiri) Selle veerus ja reas saa olema teks Hello World
SELECT 'Hello World';

--täisarvu saab küsida ilma '' märkideta 
SELECT 3;

--ei tööta PostgreSQL'is Näiteks MSS'gl töötab
SELECT 'raud'+'tee';

--Standard CONCAT töötab kõigis erinevates SQL serverites
SELECT CONCAT('all','maa','raud','tee','jaam',',',2,0,0,4);

--kui tahan erinevad veerge, siis panen väärtustele koma vahele
SELECT 'Peeter','Paat', 23, 75.45, 'Blond'

--AS märksõnaga saab anda antud veerule nime
SELECT 
	'Peeter' AS Eesnimi,
	'Paat' AS perekonnanimi, 
	23 AS vanus, 
	75.45 AS kaal, 
	'Blond' AS juuksevärv,
	
TIME:	
	
-- saab hetke kuupäeva küsida; tagastab praeguse kellaaja ja kuupäeva mingis vaikimisi formaadis
SELECT NOW();

-- Kui tahan konkreetset osa sellest, näiteks aasta
SELECT date_part('year',NOW());
-- Saab küsida kuupäeva osa ise etteentud kuupäevast 	
	SELECT date_part ('month', TIMESTAMP '2019-01-22');
	
	-- Saab küsida kuupäeva osa ise etteentud kuupäevast 
SELECT date_part ('minutes', TIME '10:10');

--Kuupäeva formaatimine Eesti kuupäeva formaati
SELECT to_char(NOW(),'HH24:mi:ss DD.MM.YYYY' );
--anname ette formaati näiteks DDxMMxYYYY 
SELECT to_char(NOW(),'HH24:mi:ss DDxMMxYYYY' );

-- Interval laseb lisada või eelmaldada mingi ajavahemisku
SELECT NOW() + interval '1 day ago';
SELECT NOW() + interval '2 centuries 3 years 2 months 1 weeks 3 days 4 seconds ';

TABEL:

--Tabeli loomine
CREATE TABLE student(
   id serial PRIMARY KEY,
	--serial tähendab et tüübiks on int mis hakkab ühe võrra suurenema
	--PRIMARY KEY tähendab, et see on primaarvõti , et see on unikaalne väli tabelis
	first_name varchar(64), 
	last_name varchar (64),
	height int,
	weight numeric(5,2),
	birthday date
);
CREATE TABLE student(
   id serial PRIMARY KEY,
	--serial tähendab et tüübiks on int mis hakkab ühe võrra suurenema
	--PRIMARY KEY tähendab, et see on primaarvõti , et see on unikaalne väli tabelis
	first_name varchar(64) NOT NULL, 
	last_name varchar (64)NOT NULL,
	height int NULL,--tohib tühi olla
	weight numeric(5,2),
	birthday date
);
-- Tabelist kõikide ridade kõikide veegude küsimine
-- * siit tähendab , et anna kõik veerud
SELECT * FROM student;

SELECT 
	first_name AS eesnimi, 
	last_name AS perekonnanimi,
		--kuidas saaks vanuse?
	date_part('year',NOW())-date_part('year', birthday) AS vanus
		
FROM
	student;
	--
SELECT  CONCAT(first_name,' ', last_name) AS täisnimi From student;

-- Kui tahan otsida /filtreerida mingi tingimuse järgi siis on WHERE otsingu sõna
SELECT 
* 
FROM 
student
WHERE
    height >= 180;
	
-- Küsi tabelist eesnime ja perekonnanime järgi mingi Peeter Tamm ja mingi Mari Maasikas
SELECT 
* 
FROM 
student
WHERE
    (first_name = 'Peeter' AND last_name = 'Tamm')
	OR 
	(first_name = 'Mari' AND last_name = 'Maasikas')

-- Anna mulle õpilased, kelle pikkus on 170-180 cm vahel
height >=170 AND height <=180
--Anna mulle õpilased, kes on pikemad kui 170 cm või lühemad kui 150cm
OR height >=170 OR height <=150
--Anna mulle õpilaste eesnimi ja pikkus, kelle sünnipäev on jaanuaris
SELECT 
first_name, height
 
FROM 
student
WHERE
     date_part ('month', birthday)= 1
	 
-- Anna mulle õpilased kelle middle_name on null (määramata)
SELECT 
   *
FROM
   student
WHERE
   middle_name IS null 
   -- või EI OLE null  -- middle_name IS NOT null 
   SELECT 
   *
FROM
   student
WHERE
   NOT (height = 180 ) --või height <> 180 või height != 180
   
   
 --Anna mulle õpilased, kelle pikkus on 069,145,180,210
SELECT 
   *
FROM
   student
WHERE
--    height = 169 OR height= 145 OR height = 180
height IN (169,145,180,210)

--Anna mulle õpilased, kelle eesnimi on Peeret, Mari või Kalle

WHERE

first_name IN ('Peeter', 'Mari','Kalle')

--Anna mulle õpilased, kelle eesnimi ei ole Peeret, Mari või Kalle
first_name NOT IN  ('Peeter', 'Mari','Kalle')
	

--Anna mulle õpilased, kelle sünnikuupäev on kuu esimene, neljas või seitsmes kuupäev
SELECT 
   *
FROM
   student
WHERE

date_part ('day', birthday) IN (1,4,7)

--Kõik WHERE võrdlused jätavad välja Null väärtusega read
SELECT 
   *
FROM
   student
WHERE

height !=180

--Anna mulle õpilase pikkuse järjekorras lühemast pikemaks

--Kui tahan tagurpidises järjekorras, siis lisandub sõna DESC (Descending)
-- Tegelikult ASC (Ascending), mis on vaikeväärtus
SELECT 
   *
FROM
   student
ORDER BY
   first_name DESC, id DESC, height, weight
   
   -- Anna mulle vanuse järjekorras vanemast nooremaks, kelle pikkus on 
   SELECT 
   height 
FROM
   student
WHERE
   height  >= 160 AND height <= 180
ORDER BY
   birthday 
   
   -- kui tahan otsida sõna seest mingit sõnaühendust K tähega algavad nimed
SELECT 
   * 
FROM
   student
WHERE
 first_name LIKE 'Ma%' --eesnimi algab Ma-ga
 OR first_name LIKE '%ee%' --eesnimi sisaldab ee-ga
 OR first_name LIKE '%aan'--eesnimi lõpeb aan-ga
 
 --Taabelisse uute kirjete lisamine
 INSERT INTO student
    (first_name, last_name, height, weight, birthday, middle_name)
VALUES
	('Alar', 'Allikas', 172, 80.55,'1980-03-14', NULL),
	('Tiiu', 'Tihane', 171, 55.55,'1994-03-12', 'Tiia'),
	('Mari', 'Maasikas', 166, 56.55,'1984-05-22', 'Marleen')
	--või 
	INSERT INTO student
    (first_name, last_name)
VALUES
	('Alar', 'Aabits')
	
-- Tabelis kirje muutmine UPDATE lausega peab olema ETTEVAATLIK
--alati peab kasutama WHERE'i lause lõpus
UPDATE
	student
SET
--UPDATE on ohtlik
	height = 185,
	weight =90,
	birthday = '1999-02-15'
WHERE 
--näiteks
-- id = 5 või
first_name = 'Peeter' AND last_name = 'Tamm'

-- Muuda kõiki õpilaste pikkus ühe võrra suuremaks
height= height+1

--Suurenda hiljem kui 1999 sündinud õpilastel sünnipäeva ühe päeva võrra
UPDATE
	student
SET
--UPDATE on ohtlik
	birthday = birthday + interval '1 days'
	WHERE
	date_part ('year',birthday)> 1999
	
	--Kustamisel olla ETTEVAATLIK. ALATI kasuta WHERE
DELETE FROM
	student
WHERE
	id > 15 --või id = 10
	
--Andmete CRUD operations
--Create (INSERT), Read (SELECT), Update (UPDATE), Delete (DELETE)

--Loo uus tabel loan, millel on väljad: amount (reaalarv), start_date, due_date, student_id, 
CREATE TABLE loan(
   id serial PRIMARY KEY,
	amount numeric(11,2) NOT NULL, 
	start_date date NOT NULL, 
	due_date date NOT NULL, 
	student_id integer NOT NULL
	);
--Lisa neljale õpilase laenud
--Kahele neist lisa veel 1 laenud
(500. NOW(),'2020-05-15', 8),

--Anna mulle kõik õpilased koos oma laenudega
--
SELECT
	*
FROM
	student
INNER JOIN -- INNER JOIN on vaikimise JOIN, INNER sõna saab ära jätta
-- INNER JOIN on selline tabelite liitmine, kus liidetakse ainult need read
-- kus on võrdsed student.id = loan.student_id--ehk need read, kus tabelite vahel on seos
-- ülejäänud ignoreeritakse
	loan
	ON student.id = loan.student_id
	--VÕI
	SELECT
	loan.*, student.*
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id
	
--Anna mulle nimed koos laenudega
SELECT
	student.first_name, student.last_name,
	loan.amount
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id
	
	--Anna mulle nimed koos laenudega
--aga ainult sellised laenud, mis on suuremad kui 500
--järjesta laenu koguse järgi suuremast väiksemaks
SELECT
	student.first_name, student.last_name,
	loan.amount
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id
WHERE
	loan.amount > 500
ORDER BY
	student.last_name,--sorteeri nime järgi
	loan.amount DESC
	
-- Loo uus tabel loan_type, milles väljad name ja description
CREATE TABLE loan_type
	(
	id serial PRIMARY KEY,
	name varchar (30) NOT NULL,
	description varchar (500) NULL
	);
	
-- uue välja lisamine olemasolevale tabelile
ALTER TABLE loan
ADD COLUMN loan_type_id int

--Tabeli kustutamine
DROP TABLE student;

--Lisatud laenu tüübid 
INSERT INTO loan_type
(name, description)
VALUES
('Õppelaen','See on väga hea laen'),
('SMS laen','See on väga halb laen'),
('Väikelaen','See on kõrge intressiga  laen'),
('Kodulaen','See on madala intressiga laen')

--Kolme tabeli INNER JOINSELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
JOIN
	loan AS l
	ON	s.id = l.student_id
JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id  
	
--INNER JOIN puhul ei ole oluline järjekord
	SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM 
	loan_type AS lt

JOIN
	loan AS l
	ON l.loan_type_id = lt.id  
	JOIN
	student AS s
	ON s.id = l.student_id   --ütleme, mis kohaga me liidame JOIN'i
	
--LEFT JOIN puhul võetakse joini esimesest (VASAKUst) tabelist kõik read
--ning parempoolsest nendele kellel infot ei ole pannakse NULL
--ning teises (Paremas) tabelis näidatakse puuduvatel kohtadel NULL
SELECT 
	s.first_name, s.last_name, l.amount
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON	s.id = l.student_id
	
-- LEFT JOIN'de  puhul on järjekord väga oluline
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON	s.id = l.student_id
LEFT JOIN 
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
-- Kui on vaja kombineerida , CROSS annab kõik kombinatsioonid kahe tabeli vahel
--kui näiteks tahame inimest panna paari, siis annab kombinatsiooni
SELECT 
	s.first_name, s.last_name, st.first_name, st.last_name
FROM
	student AS s
CROSS JOIN
	student AS st
WHERE
	s.first_name != st.first_name
	AND s.last_name != st.last_name
	
--FULL AFTER JOIN on sama kui LEFT JOIN
--saab ühe päringuga kõik komb.

SELECT 
	s.first_name, s.last_name, l.amount
FROM
	student AS s
FULL OUTER JOIN
	loan AS l
ON 	s.id = l.student_id

-- Anna mulle kõigi kasutajate perekonnanimed, kes võtsid SMS laenu ja kelle laenu kogus on üle või = 100 €. 
-- Tulemused järjesta laenu võtja vanuse järgi väiksemast suuremaks
SELECT 
	 s.last_name
FROM
	student AS s
JOIN
	loan AS l
	ON	s.id = l.student_id 
JOIN
	loan_type AS lt
	ON lt.id= l.loan_type_id   
	WHERE lt.name ='SMS laen'
	AND l.amount >=100
	ORDER BY
	s.birthday
	
--Aggregate funktsion
--Keskmise leidmine. Jäetakse välja read kus height on null
SELECT
	AVG(height)
FROM
	student
	
--Agregaatfunktsioni selectis välja kutsusdes kaob võimalus samas 
--select lauses küsida mingit muud välja tabelist, 
--sest agrfuntsiooni tulemus on alati ainult 1 number

SELECT

	AVG(COALESCE (loan.amount, 0)) --kõik kohad kus null on võrdsed 0'iga --> COALESCE (loan.amount, 0)
	
	
FROM
	student
LEFT JOIN
	loan
	ON student.id= loan. student_id
	
--
	SELECT
	ROUND(AVG(COALESCE(loan.amount, 0)),0) AS "Keskmine laenusumma",
	MIN(loan.amount) AS "Minimaalne laenusumma",
	MAX(loan.amount) AS "Maksimaalne laenusumma",
	COUNT(*) AS "Kõikide ridade arv", -- näitab kui palju ridu on tabelis
	COUNT(loan.amount) AS "Laenude arv", --jäetakse välja read, kus loan.amount on NULL
	COUNT(student.height) AS "Mitmel õpilasel on pikkus"
FROM
	student
LEFT JOIN
	loan
	ON student.id= loan. student_id
	
-- Kasutades Group BY jäävad select päringu jaoks alles vaid need väljad, mis on
-- GROUP BY's ätra toodud (s.first_name, s.last_name)
-- Teisi väljua saab ainult kasutada agregaatfunktsioonide sees
SELECT
	s.first_name, 
	s.last_name, 
	SUM(l.amount),
	MIN(l.amount),
	MAX(l.amount)
FROM
	student AS s
JOIN --kui teha LEFT JOIN tekkivad neid kellel ei ole laenu
	loan AS l
		ON s.id = l.student_id
GROUP BY
	s.first_name, s.last_name
	
	
--Anna mulle laenude summad laenu tüüpide järgi	
SELECT
	 lt.name , SUM (l.amount)
FROM
	loan AS l
JOIN 
	loan_type AS lt
		ON l.loan_type_id= lt.id
GROUP BY
	lt.name
	--Tekita mingile õpilase kaks sama tüüpi laenu
--anna mulle summad grypeerituna õpilase ning laenu tüübi kaupa
--mari ÕP 2000 (1000+1000)
--mari VL 500
--jüri ÕL 1000

	
--Anna mulle laenude summad sünniaastate järgi
SELECT
	 date_part('year', s.birthday), SUM(l.amount)
FROM
    student AS s
JOIN 
	loan AS l
		ON s.id= l.student_id
GROUP BY
	date_part('year', s.birthday)
	
	
--Anna mulle laenude summad, mis ületavad 500€, sünniaastate järgi 
SELECT
	 date_part('year', s.birthday), SUM(l.amount)
FROM
    student AS s
LEFT JOIN 
	loan AS l
		ON s.id= l.student_id
GROUP BY
	date_part('year', s.birthday)
-- Having on nagu WHERE, aga peale GROUP BY kasutamist
--Filtreerimisel saab kasutada ainult neid välja, mis on
--GROUP BY's ja agregaatfunktsioone (rakendatakse peale grupeerimist)
HAVING
	date_part('year', s.birthday) IS NOT NULL
	AND SUM (l.amount)> 500 --anna need read kus summa > 500
	AND COUNT (l.amount)=2 --anna ainult need read kus laene kokku oli 2
	-- järjestab sünniaastate järgi
ORDER BY
 date_part('year', s.birthday)-- järjestab sünniaastate järgi
 
 --kui küsime nime ka
 --Anna mulle laenude summad, mis ületavad 100€, sünniaastate järgi 
SELECT
	 date_part('year', s.birthday), s.first_name, SUM(l.amount)
FROM
    student AS s
LEFT JOIN 
	loan AS l
		ON s.id= l.student_id
GROUP BY
	date_part('year', s.birthday), s.first_name
-- Having on nagu WHERE, aga peale GROUP BY kasutamist
--Filtreerimisel saab kasutada ainult neid välja, mis on
--GROUP BY's ja agregaatfunktsioone (rakendatakse peale grupeerimist)
HAVING
	date_part('year', s.birthday) IS NOT NULL
	AND SUM (l.amount)> 100 --anna need read kus summa > 500
	AND COUNT (l.amount)>0 --anna ainult need read kus laene kokku oli 2
	-- järjestab sünniaastate järgi
ORDER BY
 date_part('year', s.birthday)-- järjestab sünniaastate järgi

--Tekita mingile õpilase kaks sama tüüpi laenu
--anna mulle summad grypeerituna õpilase ning laenu tüübi kaupa
--mari ÕP 2000 (1000+1000)
--mari VL 500
--jüri ÕL 1000
SELECT
	s.first_name, s.last_name, lt.name, SUM(l.amount) 
FROM
	student AS s
JOIN 
	loan AS l
	ON l.student_id= s.id
JOIN
	loan_type AS lt
	ON lt.id= l.loan_type_id
GROUP BY
	s.first_name, s.last_name, lt.name
	

--Mis aastal sündinud võtsid suurima summa rohkem laene?
SELECT
	date_part ('year', s.birthday), SUM (l.amount) 
FROM
	student AS s
JOIN 
	loan AS l
	ON l.student_id= s.id

GROUP BY
	date_part ('year', s.birthday)
ORDER BY
	SUM (l.amount)  DESC
LIMIT 1 --ütleb, et anna ainult esimene rida
	
-- Anna mulle mitu laenu mingist tüübist on võetud ning mis on nende summad
-- sms laen 2 1000
-- kodulaen 1 2300
SELECT
	lt.name, COUNT(l.amount), SUM(l.amount)-- COUNT'iga saab kogus COALESCE(SUM(l.amount),0)
FROM
	loan AS l --LEFT
LEFT JOIN 
	loan_type AS lt --RIGHT
	ON l.loan_type_id= lt.id
GROUP BY
	lt.name
	
--Anna mulle õpilaste eesnime esitähe esinemise statistika
--ehk siis mitu õpilase eesnimi algab mingi tähega
--nt m 3
--a on 4 õpilast
--SUBSTRING'iga saab mis sõnast, mitmendast tähest ja mitu tähte algab 1'st
SELECT 
	SUBSTRING (first_name,1,1), COUNT(SUBSTRING (first_name,1,1))-- sõna esimesed tähed
FROM
	student AS s
GROUP BY
	SUBSTRING (first_name,1,1)
	
	-- SELECT SUBSTRING('lauri mattus',  POSITION(' ' in 'lauri mattus')+1)
SELECT SUBSTRING('lauri mattus', 1, POSITION(' ' in 'lauri mattus'))

--Subquery or INNER query or Nester query
--Anna mulle õpilased, kelle pikkus vastab keskmisele õpilaste pikkusele
SELECT
first_name, last_name

FROM
student
WHERE
height=(SELECT ROUND(AVG(height)) FROM student)	

--Subquery or INNER query or Nester query
--Anna mulle õpilased, eesnimi on keskmise pikkusega õpilaste keskmine nimi

SELECT
	first_name, last_name
	FROM
		student
WHERE
	first_name IN 

(SELECT
	middle_name

FROM
student
WHERE
height=(SELECT ROUND(AVG(height)) FROM student))

--Lisa kaks vanimat õpilast töötajate tabelisse
INSERT INTO employee(first_name, last_name, birthday, middle_name)
SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday DESC LIMIT 2

Select 'Kalle', 'Kadakas', '2000-01-01', 'Jürgen'-- saab ka ise kirjutada konkreetse inimese

--teisendamine
SELECT Cast(Round (AVG (age)) As Unsigned) from emp99
	
	
	




