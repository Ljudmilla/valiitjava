package it.vali;

public class Main {
    static int a = 3;

    public static void main(String[] args) {
        int b = 7;
        System.out.println(b + a);
        a = 0;


        System.out.println(b + a);
        System.out.println(increasByA(10));
//        Shadowing , ehk sees pool defineeritud muutuja
//        varjutab väljaspool oleva muutuja
        int a = 6;

        System.out.println(b + a);
//        nii saa klassi muutuja väärtust muuta
        Main.a = 8;
//        meetodis defineeritud "a" väärtuse muutmine
        a = 5;
        System.out.println(increasByA(10));
        System.out.println();
        System.out.println(b + Main.a);
        System.out.println(b + a);
    }


    static int increasByA(int b) {
        int a = 4;
        return b += a;
    }
}
