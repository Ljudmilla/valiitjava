package it.vali;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	int a = 0;
        try {
            int b = 4/a;
            String word = null;
            word.length();

        }
//        Kui on mitu Catch plokki, siis otsib ta esimest ploki, mis oskab
//        antud Exceptionid püüda
        catch (ArithmeticException e) {
            if (e.getMessage().equals("/ by zero")){
                System.out.println("Nulliga ei saa jagada");

            }else {
                System.out.println("Esines Aritmeetiline viga");
            }
//            System.out.println(e.getMessage());

        }
        catch (RuntimeException e){
            System.out.println("Esines reaalajas esinev viga");

        }
//            Exception on klass, millest kõik erinevad Exceptioni tüübib pärinevad.
//            Mis omakorda tähendab et püüdes kinni selle üldise Exceptini, püüame kinni
//            kõik Exceptionid
        catch (Exception e) {
            System.out.println("Esines viga.");
        }
//        Küsime kasutajalt numbri ja kui number ei ole korrektses formaadis, ütleb veateate
        Scanner scanner = new Scanner(System.in);
//        boolean incorrectNumber =false;
        boolean correctNumber =false;
        do {
            System.out.println("sisesta number");
            try {
                int number = Integer.parseInt(scanner.nextLine());
                correctNumber = true;
     }
            catch (NumberFormatException e) {
                System.out.println("Number oli vigane");
    //            e.printStackTrace();

            }
        } while (!correctNumber);

//        Loo täisarvude massiiv 5 täisarvuga ning ürita sinna lisada kuues täisarv
//        Näia veateadet.
        try {
            int [] massiiv = new int [] {1,2,3,4,5};
            massiiv[5] = 6;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("indeksit, kuhu tahtsid väärtust määrata, ei eksisteeri");
//            e.printStackTrace();
        }
        catch (Exception e) {
            System.out.println("Tundmatu viga");
//            e.printStackTrace();
        }

    }


 }

