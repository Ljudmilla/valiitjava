package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
//        Kui panna üks primitiiv (value type) tüüpi muutuja võrduma teise muutujaga
//        siis tegelikult tehakse arvuti mällu uus muutuja
//        ja väärtus kopeeritakse sinna

//        Kui panna üks primitiiv (value type) tüüpi muutuja kaasa andmisel meetodi parameetriks
//        tegelikult tehakse arvuti mällu uus muutujaarvuti mällu uus muutuja
//        ja väärtus kopeeritakse sinna

        int a = 3;
        int b = a;
        a = 7;

//   Mitte statilise meetodi kutsumisel peab looma objekti, aga mitte statilise ei pea NB!!
        System.out.println(b);
        increment(b);
        System.out.println(b);
//       Kui panna  Reference tüüpi viit-tüüpi muutuja võrduma teise
//        muutujaga, siis tegelikult jääb mälus ikkagi alles ainult 1 muutuja,
//        lihtsalt teine muutuja hakkab viitama samale kohale mälus(samale muutujale)
//        Meil on kaks muutujat aga tegelikult nad on täpselt samaa objekt

//        Sama juhtub ka Reference-type viit-tüüpi muutuja kaasa andmisel meetodi parameetiks

        //teeme  massiivi

        int[] numbers = new int[]{-5};
        int[] secondNumbers = numbers;
        numbers[0] = 3;
        System.out.println(secondNumbers[0]);
        System.out.println(numbers[0]);

//   NB!     teisendamine
        int o = Integer.parseInt("3");
        String k = String.valueOf(3);


        Point pointA = new Point();
        pointA.x = 10;
        pointA.y = 3;

        Point pointB = pointA;
        pointB.x = 7;

        System.out.println(pointA.x);

        List<Point> points = new ArrayList<Point>();
        points.add(pointA);
        points.add(pointB);
//        NB!
        pointA.y = -4;

        System.out.println(points.get(0).y);
        System.out.println(points.get(1).y);
        System.out.println(pointB.y);
        System.out.println();

        System.out.println("static või mitte static");
        pointA.printNotStatic();
//        static meetodi puhul ei ole vaja objekti luua

        Point.printStatic();
        Point pointC = new Point();
        pointC.x = 12;
        pointC.y = 20;
        increment(pointC);
        System.out.println(pointC.x);
        int[] thirdNumbers = new int[]{1, 2, 3};
        increment(thirdNumbers);
        System.out.println(thirdNumbers[0]);

    }

    public static void increment(int a) {
        a++;
        System.out.printf(" a on nüüd %d%n", a);
    }

    //    suurendame x ja y koordinaate 1 võrra
    public static void increment(Point point) {
        point.x++;
        point.y++;
    }

    //    suurendame kõiki elemente 1 võrra
    public static void increment(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            numbers[i]++;

        }
        Point pointD = new Point();
        pointD.x = 5;
        pointD.y = 6;
        pointD.increment();
        System.out.println(pointD.y);

    }

}
