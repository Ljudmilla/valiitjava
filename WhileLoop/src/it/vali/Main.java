package it.vali;

import java.util.Random;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {

        // WHILE tsükkel on see kus kordI arvu ei ole teada

        // Lõpmatu tsükkel kasutame while
        // nt: iga 10 min tagant vaata kas on serveris faili?
        //     while(true) {
        //       System.out.println("Tere");
        // }
        // Programm mõtleb numbri
        // programm ütleb kasutajale: arva number ära
        // senikaua kuni kasutaja arvab valesti, ütleb: "proovi uuesti"
        Scanner scanner = new Scanner(System.in);
        do {

            Random random = new Random();
            //random.nextInt( bound: 5) genereerib numbri 0 kuni 4 ja liites +1 numbri juurde
            //int number = random.nextInt( bound: 5) + 1;
            // 80-100
            // int number = random.nextInt( 20) + 80;

            // int randomNum = ThreadLocalRandom.current().nextInt( origin: 1, bound: 6);
            //System.out.println(arva ära üks number 1 kuni 5"");
            int number = 7;
            System.out.println(" Arva ära üks number");
            int enteredNumber = Integer.parseInt(scanner.nextLine());

            while (enteredNumber != number)  {

                System.out.println("Proovi uuesti" );
                enteredNumber = Integer.parseInt(scanner.nextLine());

            }

            System.out.println("Tubli! Palju õnne!" );
            System.out.println("Kas soovid veel proovida?" );

        } while (!scanner.nextLine().toLowerCase().equals("ei"));

    }
}
