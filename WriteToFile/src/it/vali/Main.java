package it.vali;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLOutput;

public class Main {

    public static void main(String[] args) {
//        Try plokis otsitakse/oodatakse Exceptioneid (Erind, Erand, Viga)
        try {
//            FileWriter on selline klass mis tegeleb faili kirjutamisega
//            sellest klassist objekti loomisel antakse talle ette faili asukohta
//            faili asukohta võib olla ainult faili nimega kirjaldatud: output.txt
//            sel juhul kirjutatakse faili mis asub samas kaustas kus meie Main.class
//            või täispika asukohaga c : \\user\\opilane\\dokument\\output.txt
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt");
            fileWriter.write(String.format("elas metsas Mutioni%n"));

            fileWriter.write("keset kuuski elas metsas Mutionu" + System.lineSeparator());

            fileWriter.write(String.format("Kuidas elas metsas Mutionu\r\n"));
            fileWriter.close();



        //            Catch plokis püütakse kinni kindlat tüüpi Exception
//            või kõik exceptionid mis pärinevad antud exceptionist
//
        } catch (IOException e) {
//            printStackTrace tähendab et prinditakse välja meetodite välja
//            kutsumise hierarhia/ajalugu
//            e.printStackTrace();
            System.out.println("Viga: antud failile ligipääs ei ole võimalik");
        }

    }
}
