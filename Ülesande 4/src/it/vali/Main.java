package it.vali;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        // 4. Kirjuta meetod, mis võtab sisendaparameetrina aastaarvu
        // ja tagastab kõik sellel sajandil esinenud liigaastad
        // Sisestada saab ainult aastaid vahemikus 500-2019.
        // Ütle veateade, kui aastaarv ei mahu vahemikku.

        List<Integer> years = leapYearsInCentury(2019);

        for (int year : years) {
            System.out.println(year);
        }

    }

    // 1799
    // start 1700
    // end 1800
    static List<Integer> leapYearsInCentury(int year) {
        List<Integer> years = new ArrayList<Integer>();

        if (year < 500 || year > 2019) {
            System.out.println("Aasta peab olema 500 ja 2019 vahel");
            return years;
        }
//        2019 / 100 = 20 * 100 = 2000
        int centuryStart = year / 100 * 100;
        int centuryEnd = centuryStart + 99;
        int leapYear = 2020;

        for (int i = 2020; i >= centuryStart; i = i - 4) {
            if (i <= centuryEnd && i % 4 == 0) {
                years.add(i);
            }
        }
        return years;
    }


}
