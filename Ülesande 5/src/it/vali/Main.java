package it.vali;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // 5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikide nimedega
        // kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii, et see tagastab
        // riikide nimekirja eraldades komaga.
        // Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetaga ning prindi välja
        // selle objekti toString() meetodi sisu.
        Language language = new Language();
        language.setLanguageName("English");

        List<String> countryNames = new ArrayList<String>();
        countryNames.add("USA");
        countryNames.add("UK");
        countryNames.add("Australia");
        countryNames.add("India");

        language.setCountryNames(countryNames);

        System.out.println(language.toString());
    }




    }



