package it.vali;

import java.sql.SQLOutput;

public class Main {

    public static void main(String[] args) {
        // 1. Arvuta ringi pindala, kui teada on raadius
        // Prindi pindala välja erkaanile
        int r = 3;//raadius
        System.out.println("Ringi pindala on " + Math.PI * Math.pow(r, 2));
        System.out.printf("Ringi pindala on %.3f%n", Math.PI * Math.pow(r, 2));
        System.out.println();


        double radius = 24.34;
        double area = circleArea(radius);
        System.out.printf("Ringi pindala on %.2f%n", area);

        // 2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse
        // ja mille sisendparameetriteks on kaks stringi.
        // Meetod tagastab kas tõene või vale selle kohta,
        // kas stringid on võrdsed

        String firstString = "kala";
        String secondString = "maja";
        boolean areEqual = equals(firstString, secondString);
        System.out.printf("Sõnad %s ja %s on võrdsed: %s%n", firstString, secondString, areEqual);
//        if(areEqual) {
//            System.out.println("Sõnad on võrdsed");
//        }
//        else {
//            System.out.println("Sõnad ei ole võrdsed");
//        }


        // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude
        // massiiv ja mis tagastab stringide massivi.
        // Iga sisend-massiivi elemendi kohta olgu tagastatavas massiivis samapalju a tähti.
        // 3, 6, 7
        // "aaa", "aaaaaaa", "aaaaaaa"
        int[] numbers = new int[]{3, 6, 7};
        String[] words = intArrayToStringArray(numbers);
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }

    }

    static double circleArea(double radius) {
        return Math.PI * Math.pow(radius, 2);
    }

    static boolean equals(String firstString, String secondString) {
        if (firstString.equals(secondString)) {
            return true;
        }
        return false;
        // return firstString.equals(secondString);
    }

    static String[] intArrayToStringArray(int[] numbers) {
        String[] words = new String[numbers.length];
//        numbers[0] = 3; words[0] = "aaa";
//        numbers[1] = 4; words[1] = "aaaa";
//        numbers[2] = 7; words[2] = "aaaaaaa";

        for (int i = 0; i < words.length; i++) {
            words[i] = genereteAString(numbers[i]);
        }

        return words;
    }

    static String genereteAString(int number) {
        String wordA = "";

        for (int i = 0; i < number; i++) {
            wordA = wordA + "a";
        }

        return wordA;
    }
}
